!
! Copyright (c) 2011-2015 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

module lap_hank_finite_soln
  implicit none

  private
  public :: unconfined_finite_wellbore_slug

contains

  function unconfined_finite_wellbore_slug(dum,bD,dD,lD,alphaD,rDw,CD,kappa,lap) result(fp)
    use constants, only : EP,DP,EONE
    use inverse_Laplace_Transform, only : dehoog_pvalues
    use complex_bessel, only : cbesk
    use inverse_Laplace_transform, only : INVLT

    implicit none
    real(EP), intent(in) :: dum  ! scalar integration variable (Hankel parameter)
        real(DP), intent(in) :: bD, lD, dD, rDw, CD
    real(DP), intent(in) :: alphaD, kappa
    type(INVLT), intent(in) :: lap
    complex(EP), dimension(2*lap%M+1) :: fp

    complex(EP), dimension(2*lap%M+1) :: eta, eps, wD, xiw
    complex(DP), dimension(2*lap%M+1) :: K1
    complex(EP), dimension(0:2,2*lap%M+1) :: delta
    real(EP) :: lDs,dDs
    integer :: i, np,nz,ierr
    logical :: nans

    NaNs = .false.
    np = 2*lap%M+1

    ! pre-compute some intermediate values
    eta(1:np) = sqrt((lap%p(:) + dum**2)/kappa)
    eps(1:np) = lap%p(:)/(eta(:)*alphaD)
    dDs = EONE - real(dD,EP)
    lDs = EONE - real(lD,EP)
    xiw = sqrt(lap%p(:))*rDw
    
    do i=1,np
       call cbesk(z=cmplx(xiw(i),kind=DP),fnu=1.0D0,kode=1,n=1,cy=K1(i),nz=nz,ierr=ierr)
       if (ierr /= 0 .and. ierr /= 3) then
          write(*,*) i,'CBESK error (lap_hank_fin_soln)',ierr,' nz=',nz, ' z=',xiw(i)
       end if
    end do

222 continue

    if ((.not. NaNs) .and. maxval(abs(cosh(eta))) < huge(1.0_EP)) then

       ! when this overflows double precision, switch to approximate form
       delta(0,1:np) = sinh(eta) + eps*cosh(eta)
       delta(1,1:np) = sinh(eta*dD) + eps*cosh(eta*dD)
       delta(2,1:np) = sinh(eta*lD) + eps*cosh(eta*lD)
    
       ! < \hat{ \bar{ w }}_D>
       wD(1:np) = (delta(1,:)*sinh(eta*dDs) + (delta(2,:)-2.0*delta(1,:))*sinh(eta*lds)) / &
            & (bD*eta(:)*delta(0,:))
    else
       ! large argument form (large p or a)
       wD(1:np) = (1.0 - exp(-eta*bD))/(bD*eta)
    end if
    
    ! \hat{ \bar{ \Omega }}  
    fp(1:np) = CD*(EONE - wD(:))/((lap%p+dum**2)*xiw(:)*K1(:))

    if (any(isnan(abs(fp)))) then
       NaNs = .true.
       goto 222
    end if
    
    ! solution always evaluated in test well (rDw)

  end function unconfined_finite_wellbore_slug
end module lap_hank_finite_soln




