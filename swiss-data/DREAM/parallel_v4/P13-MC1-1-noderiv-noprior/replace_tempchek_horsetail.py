parfn = 'parameters.txt'
fh = open(parfn,'r')
lines = fh.readlines()
fh.close()

x = []
for line in lines:
    x.append(float(line.strip()))

ll,le,ss,k1,kr,sy,leo,llo = x

ptf = """F  F  F                  	     :: suppress output to screen?, output dimensionless t?, log output?
5.81  F  %.8g  %.8g               :: saturated thickness (B), compute L/Le?, L, Le
0.0065 T 7.82 5.87   %.8g  %.8g    :: robs, computeOmegao, omegao, gammao, LLeo, LLo
3.9  5.11              :: r, z observation locations     
5.125  4.775     	     :: l,d; depth to bottom, top of test interval
0.0315  0.0065     	     :: rw,rc; wellbore, tubing radius
%.8g  %.8g  %.8g  0.02 3.87 0.01   %.8g  :: K1,2,3;del1,2,3;Kz
%.8g  %.8g         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
25  1.0D-12  1.0D-13           :: deHoog invlap;  M,alpha,tol
7  5                         :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
1  1  10  50                :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
-2  2  100                   :: logspace times; lo, hi, # times to compute
F  75  horsetail75-time.dat             :: compute times?, # times to read, filename for times (1 time/line)
test1obs.out
"""

fh = open('input-gen.dat','w')
fh.write(ptf % (ll,le,leo,llo,k1,kr,k1,kr,ss,sy))
fh.close()
