
set(gcf, 'visible','off');
clear;

deriv = 0;

pest_results =  ...
   [3.91256           -6.55367           14.3788; ...
    5.90970            5.58321           6.23620; ...
    6.134992E-05      -1.676176E-04      2.903175E-04; ...
    0.570835          -0.679407           1.82108; ...
    6.828524E-04      -4.910764E-04      1.856781E-03; ...
    0.400000           -1.91437           2.71437; ...
    9.771156E-02      -4.216074E-02      0.237584; ...
    5.09448           -5.54706           15.7360;];

% pest_results has 8 rows (8 parameters: ll, le, ss, k1, kr, sy, leo, llo)
% and three columns (estimated, 95% lower, 95% upper)

load 'DREAM_ZS.mat';

prefix = 'P13MC11-noderiv-noprior-fulldata';

% width of skins and domain (del_1, del_2, del_3)
del = [0.02, 3.87, 0.01];
delT = del(1) + del(2) + del(3);

% paramter 4 (k1) is a multiplier on parameter 5 (kr)
Sequences(:,4:5,:) = 10.0.^Sequences(:,4:5,:);
Sequences(:,4,:) = Sequences(:,4,:).*Sequences(:,5,:);

% parameter 4 (K1) is K_skin (same skin on signal & monitoring wells: K1=K3)

% parameter 5 (Kr) is changed to parallel averaged <Kr>, which is computed from
% <Kr> = delT/(del_1/K1 + del_2/Kr + del_3/K1)

Sequences(:,5,:) = delT./(del(1)./Sequences(:,4,:) + ...
                          del(2)./Sequences(:,5,:) + ...
                          del(3)./Sequences(:,4,:));

Sequences(:,4:5,:) = log10(Sequences(:,4:5,:));                      

% apply same conversion to pest results                       
pest_results(5,:) = delT./(del(1)./pest_results(4,:) + ...
                           del(2)./pest_results(5,:) + ...
                           del(3)./pest_results(4,:));
                       
pest_results(3:5,:) = log10(pest_results(3:5,:));
for i = 3:5
    if abs(imag(pest_results(i,2))) > 1.0E-8
        % convert complex to make log-scale plot make sense
        pest_results(i,2) = -999;
    end
end
                      
nchains = size(Sequences,3);
npar = size(Sequences,2)-2;
niter = 13500;
%niter = size(Sequences,1);

vars = {'L','L_e','log_{10}(S_s)','log_{10}(K_{skin})', ...
        'log_{10}(<K>)','S_y','L_{e,obs}','L_{obs}'};

colors = {'r','g','b','c','m','k','y','r'};

% plot of chains
% ***************
if 1
    figure();
    for i = 1:npar
        subplot(npar,1,i);
        % plot each chain as a differnt colors
        for j =1:nchains
            plot(Sequences(:,i,j),[colors{j} '-']);
            hold on;
        end
        ylabel(vars{i},'FontSize',12);
        xlim([0,niter]);
    end

    xlabel('iterations per chain','FontSize',9);
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-all-chains.eps']);
end
    
% reshape output after burnin period
burnin = 5000;
jump = niter-burnin;
tmp = zeros(npar+1,jump*nchains,1);
for i = 1:npar
    for j=1:nchains
        tmp(i,jump*(j-1)+1:j*jump) = Sequences(burnin+1:niter,i,j);
    end
end

tmp(tmp == 0.0) = NaN;

% plot of histograms
% ******************

nbins = 25;

if 1
    figure();
    for i = 1:npar
        subplot(4,2,i);
        % normalized histogram  from 
        % http://www.mathworks.com/matlabcentral/fileexchange/22802-normalized-histogram
        xx = tmp(i,:);
        [n,xout] = histnorm(xx,nbins);
        m = mean(xx(~isnan(xx)));
        v = var(xx(~isnan(xx)));
        hbar = bar(xout,n,'BarWidth',1.0);
        hold on;
        xlabel(vars{i},'FontSize',12);
        ylabel('Posterior density','FontSize',9);
        
        yl = ylim();
        xl = xlim();
        
        % red vertical line showing PEST estimate
        hline = plot(pest_results(i,[1,1]),[0,1.0E+6], 'LineStyle','-', 'Color',[1 0 0]);
        
        % green box indicating PEST 95% confidence interval
        x = [pest_results(i,2),pest_results(i,2), ...
             pest_results(i,3),pest_results(i,3)];
        y = [0,                1.0E+6,           ...
             1.0E+6,           0];
        fill(x,y,'g','FaceAlpha',0.5);
        
        % fix vertical order of objects in figure
        uistack(hbar,'top');
        uistack(hline,'top')
        
        % reset scales
        ylim([0,yl(2)]);
        xlim(xl);
  
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-dpng',[prefix,'-posterior-histograms.png']);
end

% plot of correlation between posterior distributions
% ***************************************************
if 1
    figure();
    [H,AX,BigAx,P,PAx] = plotmatrix(transpose(tmp(1:npar,:)),'r.');
    for i = 1:npar
       title(BigAx,'Pariwise distributions of DREAM results')
        ylabel(AX(i,1),vars{i},'FontSize',9);
        xlabel(AX(npar,i),vars{i},'FontSize',9);
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-posterior-joint-distributions.eps']);
end

% horsetail plot (must be re-computed and takes a while)
% *******************************************************
if 1
    nhorsetail = 5;  % re-run model nhorsetail*nchains times

    nt = 75;  % this must be same as that in python script
    times = linspace(min(Extra.t),max(Extra.t),75)';
    Struct.times = times;

    if deriv == 0
        z = load('full-data-and-derivative.csv');

        % swap sign on data (and its derivative)
        Measurement.MeasData = [-z(:,1);-z(:,2)];
    end

    
    NT = size(Extra.t,1);

    p = zeros(8,1);
    
    figure();
    for i = 1:nhorsetail
        for j = 1:nchains
            disp((i-1)*nchains + j);
            
            % write parameters into form expected by genslug
            p(1:8) = Sequences(niter-i,1:8,j);  % L and Le not log-transformed
            %p(4) = p(4)/p(5);    % skin perm is multiplier (undo back transform at top of this file)
            p(3:5) = 10.0.^p(3:5);
            %p(4) = p(4)*p(5);
            
            %p(6:8) = Sequences(niter-i,6:8,j);


            save('parameters.txt', 'p', '-ascii');
            unix('python ./replace_tempchek_horsetail.py');
            unix('./matlab_genslug.sh');
            unix('sed -i "1,18d" ./test1obs.out');

            z = load('test1obs.out');
            S = [z(:,2);z(:,3)];

            subplot(121);
            plot(times,S(1:nt),'k-');
            hold on;
            subplot(122);
            plot(times,S(nt+1:end),'k-');
            hold on;
        end
    end
    subplot(121);
    plot(Extra.t,Measurement.MeasData(1:NT),'ro');
    xlabel('time [seconds]');
    ylabel('Head [m]');
    grid on;
    xlim([0,17]);
    ylim([-0.005,0.005]);

    subplot(122);
    if deriv
        plot(Extra.t,Measurement.MeasData(NT+1:end),'ro');
    else
        plot(Extra.t,Measurement.MeasData(NT+1:end),'gx');
    end
    xlabel('time [seconds]');
    ylabel('\partial Head / \partial log(t) [m]');
    grid on;
    xlim([0,17]);
    ylim([-0.015,0.015]);
            
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-horsetail.eps']);
    
end