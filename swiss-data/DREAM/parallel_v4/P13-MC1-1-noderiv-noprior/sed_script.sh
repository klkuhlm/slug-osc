#!/bin/bash

IFS=$'\n' :; PARS=($(cat parameters.txt))
## echo "Ll ${PARS[0]}" "Le ${PARS[1]}" "Ss ${PARS[2]}" "k1 ${PARS[3]}" "kr ${PARS[4]}" "Sy ${PARS[5]}" "Leo ${PARS[6]}" "Llo ${PARS[7]}"

#   0  1  2  3  4  5  6   7
## ll,le,ss,k1,kr,sy,leo,llo 

sed -e "s/%%%%Ll%%%%/${PARS[0]}/" \
    -e "s/%%%%Le%%%%/${PARS[1]}/" \
    -e "s/%%%%Ss%%%%/${PARS[2]}/" \
    -e "s/%%%%k1%%%%/${PARS[3]}/g" \
    -e "s/%%%%kr%%%%/${PARS[4]}/g" \
    -e "s/%%%%Sy%%%%/${PARS[5]}/" \
    -e "s/%%%%Leo%%%%/${PARS[6]}/" \
    -e "s/%%%%Llo%%%%/${PARS[7]}/" < sed_template.txt > input-gen.dat

