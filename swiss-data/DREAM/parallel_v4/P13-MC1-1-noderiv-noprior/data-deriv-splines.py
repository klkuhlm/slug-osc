from scipy.interpolate import UnivariateSpline as spline
import numpy as np
import matplotlib.pyplot as plt
from glob import glob

headweight = 1.0
derivweight = 0.4

for df in glob('obs_well-P13-MC1-1.dat'):
    
    print 'loading:',df
    tmin = 0.0
    
    t,c = np.loadtxt(df,delimiter=',',unpack=True,skiprows=1)
    
    m = t > tmin
    tt = t[m]
    cc = c[m]

    if 'obs' in df:
        tp = tt[::4]  # downsample uniformly
        pp = cc[::4]
        ##if '1' in df:
        ##    factor = 130.0
        ##elif '2' in df:
        ##    factor = 80.0
        ##else:
        ##    factor = 150.0
        ## MC1-5 --> factor = 140.0
        factor = 120.0
    else:    
        # only downsample late time
        em = tt < 1.75
        lm = tt > 1.75
        tp = np.concatenate((tt[em],tt[lm][::10]),axis=0)
        pp = np.concatenate((cc[em],cc[lm][::10]),axis=0)
        factor = t.shape[0]/4  # <<-- fudge factor for spline tension        

    logt = np.log(tt)
    nt = tp.shape[0]
    
    s1fcn = spline(logt,cc,s=np.linalg.norm(cc)/factor)
    
    sp1 = np.empty((nt,2))
    for j in range(nt):
        sp1[j,:] = s1fcn.derivatives(np.log(tp[j]))[0:2]
    
    fig = plt.figure(1)
    ax = fig.add_subplot(211)
    ax.plot(tt,cc,'r.')
    ax.plot(tp,pp,'k*')
    ax.plot(tp,sp1[:,0],'k-')
    ax.set_xlabel('time (sec)')
    ax.set_ylabel('normalized head')
    ax2 = ax.twinx()
    ax2.plot(tp,sp1[:,1],'g--')
    ax.set_xlim([tt[0],tt[-1]])
    ax2.set_ylabel('d conc/d(ln(t))')
    
    ax = fig.add_subplot(212)
    ax.semilogx(tt,cc,'r.')
    ax.semilogx(tp,pp,'k*')
    ax.semilogx(tp,sp1[:,0],'k-')
    ax.set_xlabel('time (sec)')
    ax.set_ylabel('normalized head')
    ax2.set_title(df.replace('.dat','') + ' factor=%.3g' %factor)
    ax2 = ax.twinx()
    ax2.semilogx(tp,sp1[:,1],'g--')
    ax2.set_xlim([tt[0],tt[-1]])
    ax2.set_ylabel('d conc/d(ln(t))')
    
    output = np.concatenate((tp[:,None],sp1[:,1][:,None]),axis=1)
    np.savetxt(df.replace('.dat','.txt'),output,fmt='%.5g',delimiter='\t')
    
    plt.savefig(df.replace('.dat','.png'))
    plt.close(1)

    # create single PEST instruction file for reading model outputs
    fh = open(df.replace('.dat','.ins'),'w')
    fh.write('@pif\n@----------------------------@\n')
    for j in range(nt):
        fh.write('l1 [obs%3.3i]28:50 [dbs%3.3i]53:75\n' % (j+1,j+1))
    fh.close()

    # create single PEST instruction file for reading model outputs
    fh = open(df.replace('.dat','_noderiv.ins'),'w')
    fh.write('@pif\n@----------------------------@\n')
    for j in range(nt):
        fh.write('l1 [obs%3.3i]28:50 \n' % (j+1))
    fh.close()

    # create data section of PEST control file
    fh = open(df.replace('.dat','_data_section.pst'),'w')
    fh.write('* observation data\n')
    for j in range(nt):
        fh.write('obs%3.3i  %.5g  %.2f obsgroup\n' % (j+1,pp[j],headweight))
    for j in range(nt):
        fh.write('dbs%3.3i  %.5g  %.2f dbsgroup\n' % (j+1,sp1[j,1],derivweight))
    fh.close()

    # create "time" file for forward model
    fh = open(df.replace('.dat','_time.dat'),'w')
    for j in range(nt):
        fh.write('%.5g\n' % (tp[j]))
    fh.close()
