
set(gcf, 'visible','off');
clear;

nd = load('DREAM_ZS.mat');
d = load('../P13-MC1-1-Wderiv10-noprior/DREAM_ZS.mat');

prefix = 'P13MC11-deriv-and-noderiv';

% paramter 4 (k1) is a multiplier on parameter 5 (kr)
nd.Sequences(:,4:5,:) = 10.0.^nd.Sequences(:,4:5,:);
nd.Sequences(:,4,:) = nd.Sequences(:,4,:).*nd.Sequences(:,5,:);
nd.Sequences(:,4:5,:) = log10(nd.Sequences(:,4:5,:));

d.Sequences(:,4:5,:) = 10.0.^d.Sequences(:,4:5,:);
d.Sequences(:,4,:) = d.Sequences(:,4,:).*d.Sequences(:,5,:);
d.Sequences(:,4:5,:) = log10(d.Sequences(:,4:5,:));

nd.nchains = size(nd.Sequences,3);
nd.npar = size(nd.Sequences,2)-2;
%nd.niter = 22250;
nd.niter = size(nd.Sequences,1);

d.nchains = size(d.Sequences,3);
npar = size(d.Sequences,2)-2;
d.niter = 44400;
%d.niter = size(d.Sequences,1);

vars = {'L_L','L_e','log_{10}(S_s)','log_{10}(K_1)', ...
        'log_{10}(K_r)','S_y','L_{eo}','L_{Lo}'};

colors = {'r','g','b','c','m','k','y','r'};
   
% reshape output after burnin period
nd.burnin = 40000;
nd.jump = nd.niter-nd.burnin;
nd.tmp = zeros(npar+1,nd.jump*nd.nchains,1);
for i = 1:npar
    for j=1:nd.nchains
        nd.tmp(i,nd.jump*(j-1)+1:j*nd.jump) = nd.Sequences(nd.burnin+1:nd.niter,i,j);
    end
end

nd.tmp(nd.tmp == 0.0) = NaN;

d.burnin = 20000;
d.jump = d.niter-d.burnin;
d.tmp = zeros(npar+1,d.jump*d.nchains,1);
for i = 1:npar
    for j=1:d.nchains
        d.tmp(i,d.jump*(j-1)+1:j*d.jump) = d.Sequences(d.burnin+1:d.niter,i,j);
    end
end

d.tmp(d.tmp == 0.0) = NaN;


% plot of histograms
% ******************

nbins = 25;

if 1
    figure();
    for i = 1:npar
        subplot(4,2,i);
        % normalized histogram  from 
        % http://www.mathworks.com/matlabcentral/fileexchange/
        % 22802-normalized-histogram
        xx = nd.tmp(i,:);
        [n,xout] = histnorm(xx,nbins);
        m = mean(xx(~isnan(xx)));
        v = var(xx(~isnan(xx)));
        bar(xout,n,'b','BarWidth',1.0);
        
        hold on;
        
        xx = d.tmp(i,:);
        [n,xout] = histnorm(xx,nbins);
        m = mean(xx(~isnan(xx)));
        v = var(xx(~isnan(xx)));
        bar(xout,n,'r','BarWidth',1.0);
        alpha(0.5)

        xlabel(vars{i},'FontSize',12);
        ylabel('Posterior density','FontSize',9);
        
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-dpng',[prefix,'-posterior-histograms.png']);
end


