#!/bin/bash 

# switch to modern gcc compilers
module unload intel
module load gnu/4.7.2

# switch to openmpi compatible with gcc
module unload openmpi-intel
module load openmpi-gnu/1.6


for i in {1..8}; do

    DIR=$(printf '%2.2i' ${i}) # two-digit directories
    #NODE=$((i-1))  # these are zero-based

    cd ${DIR}

    # write input file for genslug with parameter values in 
    # parameters.txt substituted into the input file
    ./sed_script.sh 

    # look for DONE file to see if done, delete old version
    rm -f test1obs.out.DONE test1obs.out
      
    cd ..

done

mpiexec --mca mpi_warn_on_fork 0 --n 8 --pernode ./run_genslug 
