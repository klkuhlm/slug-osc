#!/bin/bash

# matlab resets this to point to its libraries (thanks a heap)
export LD_LIBRARY_PATH=''

export OMP_NUM_THREADS=18

# call my program
./genslug input-gen.dat 2>&1 > genslug.dbg
