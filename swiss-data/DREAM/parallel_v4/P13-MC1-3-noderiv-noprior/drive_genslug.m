function [ S ] = drive_genslug(Pars,Extra)
%drive_multirate: this script calls the Fortran code
%           that computes the oscillating water table slug response

% L_l = Pars(1);     % pumping well factor
% L_e = Pars(2);     % pumping well factor
% S_s = Pars(3);     % formation specific storage
% k_1 = Pars(4);     % skin hydraulic conductivity
% k_r = Pars(5);     % formation radial hydraulic conductivity
% S_y = Pars(6);     % formation specific yield
% Le_o = Pars(7);    % observation well factor
% Ll_o = Pars(8);    % observation well factor

%  global saveruns;
  global idx;
  format compact;

  if Pars(5) > 2*Pars(4)

    % only want a more permeable skin
    S = -999999*ones(2*size(Extra.t,1),1);

  else
    % un-log transform parameters
    p = zeros(8,1);
    p(1:2) = Pars(1:2);  % L and Le not log-transformed
    p(3:5) = 10.0.^Pars(3:5);
    p(6:8) = Pars(6:8);

    save('parameters.txt', 'p', '-ascii');
    
    cmd='./drive-genslug.sh';
    unix(cmd);

    z = load('./test1obs.out');
    fprintf('%5i %.3g %.3g %.3g %.3g %.3g %.3g %.3g %.3g\n', ...
                 idx,p(1),p(2),p(3),p(4),p(5),p(6),p(7),p(8));
    

    S = [z(:,2);z(:,3)]; % time, pressure, derivative
    
    %%% save parameters and model output
    %%saveruns(idx,1:8) = p(1:8);
    %%saveruns(idx,9:end) = S;
    idx = idx + 1;
  end
end
