program run_genslug
implicit none
include 'mpif.h'

integer :: ierr, p, id
character(40) :: cmd

call MPI_Init(ierr)
if (ierr /= 0) write(*,'(A)') 'error w/ MPI_Init'

call MPI_Comm_size(MPI_COMM_WORLD, p, ierr)
if (ierr /= 0) write(*,'(A,I0)') 'error w/ MPI_Comm_size ',p

call MPI_Comm_rank(MPI_COMM_WORLD, id, ierr)
if (ierr /= 0) write(*,'(A,I0)') 'error w/0 MPI_Comm_rank ',id

cmd = './genslug XX/input-gen.dat 2>&1 > XX/dbg'
write(cmd(11:12),'(I2.2)') id+1
write(cmd(35:36),'(I2.2)') id+1

!!write(*,'(A)') 'preparing to execute '//cmd
call execute_command_line(cmd, wait=.false.)
!!write(*,'(A)') 'RUN_GENSLUG: executed '//cmd

call MPI_Finalize(ierr)
if (ierr /= 0) write(*,'(A)') 'error w/ MPI_Finalize'

end program run_genslug
