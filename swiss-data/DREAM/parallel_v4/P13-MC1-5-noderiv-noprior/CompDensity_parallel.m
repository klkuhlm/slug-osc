function [p,log_p] = CompDensity_parallel(x,MCMCPar,Measurement,ModelName,Extra,option)
% This function computes the density of each x value

p = []; 
log_p = [];

% assumes there are a number of directories named 01, 02, 03, ...., N
% (N = total number of chains), where each model run is executed in parallel

% step 1: write input files for N chains into run directories
% step 2: call bash script that executes on N nodes using mpiexec
%         while each forward run is using 8 cores on each node
%         code writes empty file with ".DONE" appended to input name when finished
% step 3: loop through directories, checking for "DONE" file and
%         loading results
% 

% step 1: write inputs into each directory
for ii = 1:size(x,1)
    Extra.dirname = sprintf('%2.2i',ii);
    write_genslug_inputs_parallel(x(ii,:),Extra);
end
    
% step 2: spawn off N versions of genslug and exit
unix('./run_genslug.batch');

% Loop over the individual parameter combinations of x
% reading in output files from directories
for ii = 1:size(x,1)

    d = sprintf('%2.2i',ii); # zero-padded directory

    % check for file indicating bad inputs
    if exist([d,'/DONOTRUN'],'file')
        % large nonsense output
        ModPred = -999999*ones(2*size(Extra.t,1),1);
        % remove indicator file
        unix(['rm -f ',d,'/DONOTRUN']);
    else

        donefile = [d,'/input-gen.dat.DONE'];
        outfile  = [d,'/test1obs.out'];
    
        % wait for DONE file
        while ~exist(donefile,'file')
            pause(0.1); 
        end
	unix('sed -i "1,18d" test1obs.out');
    
        % load output file
        z = load(outfile);
        ModPred = [z(:,2);z(:,3)];
    end

    % assumes "option == 3"
    Err = (Measurement.MeasData(:)-ModPred(:));
    % Derive the sum of squared error
    SSR = sum(Err.^2);
    % And retain in memory
    p(ii,1) = -SSR; 
    log_p(ii,1) = -0.5 * SSR;

end

