% ------------- DREAM with sampling from past and snooker updates: DREAM_ZS --------------------%
%                                                                                               %
% The code presented herein is a Markov Chain Monte Carlo algorithm that runs multiple chains   %
% in parallel for efficient posterior exploration. The algorithm, entitled DREAM_(ZS) is        %
% based on the original DREAM sampling scheme, but uses sampling from an archive of past        %
% states to generate candidate points in each individual chain. Theoy and numerical examples of %
% DREAM_(ZS) have been presented in Vrugt et al. (2009). Details can also be found in           %
% Ter Braak and Vrugt (2008)                                                                    %
%                                                                                               %
% Sampling from past has three main advantages:                                                 %
% (1) Circumvents the requirement of using N = d for posterior exploration. This will speed-up  %
% convergence to a limiting distribution, especially for high-dimensional problems (large d).   %
% (2) Outlier chains do not need explicit consideration. By sampling historical states,         %
% aberrant trajectories an jump directly to the modal region at any time during the             %
% simulation. The N path ways simulated with DREAM_(ZS) therefore maintain detailed balance at  %
% every singe step in the chain.                                                                %
% (3) The transition kernel defining the jumps in each of the chains does not require           %
% information about the current states of the chains. This is of great advantage in a           %
% multi-processor environment where the N candidate points can be generated simultaneously so   %
% that each chain can evolve most efficiently on a different computer. Details of this will be  %
% given in a later publication, which should be ready within the next few months.               %
%                                                                                               %
% DREAM_(ZS) also contains a snooker updater to maximize the diversity of candidate points      %
% and generate jumps beyond parallel direction updates. Finally, DREAM_(ZS) contains subspace   %
% learning in a similar way as DREAM, to maximize the squared jumping distance between two      %
% subsequent points in each chain. This idea has been presented in Vrugt et al. (2008) and      %
% shown to significantly increase the efficiency of posterior exploration. All these options    %
% can be activated from the input file.                                                         %
%                                                                                               %
% DREAM_(ZS) developed by Jasper A. Vrugt and Cajo ter Braak                                    %
%                                                                                               %
% This algorithm has been described in:                                                         %
%                                                                                               %
%   C.J.F. ter Braak, and J.A. Vrugt, Differential Evolution Markov Chain with snooker updater  %
%       and fewer chains, Statistics and Computing, 10.1007/s11222-008-9104-9, 2008             %
%                                                                                               %
%   Vrugt, J.A., and C.J.F. ter Braak, DiffeRential Evolution Adaptive Metropolis with Sampling %
%       from the Past and Subspace Updating, SIAM journal on Optimization                       %
%                                                                                               %
%   Vrugt, J.A., and C.J.F. ter Braak, Multiple Try DiffeRential Evolution Adaptive Metropolis  %
%       for High Performance Computing, SIAM Journal on Distributed Computing                   %
%                                                                                               %
% For more information please read:                                                             %
%                                                                                               %
%   Vrugt J.A., H.V. Gupta, W. Bouten and S. Sorooshian, A Shuffled Complex Evolution           %
%       Metropolis algorithm for optimization and uncertainty assessment of hydrologic model    %
%       parameters, Water Resour. Res., 39 (8), 1201, doi:10.1029/2002WR001642, 2003.           %
%                                                                                               %
%   ter Braak, C.J.F., A Markov Chain Monte Carlo version of the genetic algorithm Differential %
%       Evolution: easy Bayesian computing for real parameter spaces, Stat. Comput., 16,        %
%       239 - 249, doi:10.1007/s11222-006-8769-1, 2006.                                         %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, M.P. Clark, J.M. Hyman, and B.A. Robinson, Treatment of      %
%       input uncertainty in hydrologic modeling: Doing hydrology backward using Markov         %
%       chain Monte Carlo, Water Resour. Res., 44, W00B09, doi:10.1029/2007WR006720, 2008.      %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, C.G.H. Diks, D. Higdon, B.A. Robinson, and J.M. Hyman,       %
%       Accelerating Markov chain Monte Carlo simulation by self adaptive differential          %
%       evolution with randomized subspace sampling, International Journal of Nonlinear         %
%       Sciences and Numerical Simulation, In Press. 2009.                                      %
%                                                                                               %
% Copyright (c) 2008, Los Alamos National Security, LLC                                         %
%                                                                                               %
% All rights reserved.                                                                          %
%                                                                                               %
% Copyright 2008. Los Alamos National Security, LLC. This software was produced under U.S.      %
% Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is     %
% operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S.     %
% Government has rights to use, reproduce, and distribute this software.                        %
%                                                                                               %
% NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES A NY WARRANTY, EXPRESS OR  %
% IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE. If software is modified to    %
% produce derivative works, such modified software should be clearly marked, so as not to       %
% confuse it with the version available from LANL.                                              %
%                                                                                               %
% Additionally, redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:                   %
% * Redistributions of source code must retain the above copyright notice, this list of         %
%   conditions and the following disclaimer.                                                    %
% * Redistributions in binary form must reproduce the above copyright notice, this list of      %
%   conditions and the following disclaimer in the documentation and/or other materials         %
%   provided with the distribution.                                                             %
% * Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL %
%   the U.S. Government, nor the names of its contributors may be used to endorse or promote    %
%   products derived from this software without specific prior written permission.              %
%                                                                                               %
% THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND   %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES      %
% OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS %
% ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, %
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF   %
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)        %
% HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,       %
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                            %
%                                                                                               %
%                                                                                               %
% Copyright (c) 2008, Los Alamos National Security, LLC                                         %
%                                                                                               %
% Written by Jasper A. Vrugt: vrugt@lanl.gov                                                    %
%                                                                                               %
% Version 0.5: January 2009                                                                     %
% Version 1.0: April 2011         Maintenance update, explicit treatment of prior distribution  %
% Version 1.1: August 2011        Whittle likelihood function (SPECTRAL ANALYSIS !!)            %
%                                                                                               %
% --------------------------------------------------------------------------------------------- %

% Different test examples from SIAM paper
% example 1: n-dimensional Gaussian distribution
% example 2: multivariate student t distribution
% example 3: n-dimensional banana shaped Gaussian distribution
% example 4: n-dimensional multimodal mixture distribution
% example 5: real-world example using hymod rainfall - runoff model (HYMOD code in MATLAB)
% example 6: real-world example using hymod rainfall - runoff model (HYMOD code in FORTRAN)
% example 7: rainfall-runoff model with generalized log-likelihood function
% example 8: HYDRUS-1D soil hydraulic model: using prior information on soil hydraulic parameters

	
% -------------------------------- Check the following paper ------------------------------ %
%                                                                                           %
%   B. Scharnagl, J.A. Vrugt, H. Vereecken, and M. Herbst (2011), Bayesian inverse          % 
%	modeling of soil water dynamics at the field scale: using prior information             % 
%	on soil hydraulic properties, Hydrology and Earth System Sciences.                      %  
%                                                                                           %
% ----------------------------------------------------------------------------------------- % 

% Problem specific parameter settings
MCMCPar.n = 8;                          % Dimension of the problem
MCMCPar.ndraw = 600000;                   % Maximum number of function evaluations
MCMCPar.parallelUpdate = 0.9;           % Fraction of parallel direction updates
MCMCPar.pJumpRate_one = 0.20;           % Probability of selecting a jumprate of 1 --> jump between modes

% Recommended parameter settings
MCMCPar.seq = 8;                        % Number of Markov Chains / sequences
MCMCPar.DEpairs = 1;                    % Number of chain pairs to generate candidate points 
MCMCPar.nCR = 3;                        % Number of crossover values used (nCR >= 2)
MCMCPar.m0 = 10 * MCMCPar.n;            % Initial size of Z
MCMCPar.k = 10;                         % Thinning parameter for appending X to Z
MCMCPar.eps = 5e-2;                     % Perturbation for ergodicity
MCMCPar.steps = 10;                     % Number of steps before calculating convergence diagnostics

% --------------------------------------------------------------------------------------------
Extra.pCR = 'Update';                   % Adaptive tuning of crossover values
% --------------------------------------------------------------------------------------------

% --------------------------------------- Added for reduced sample storage -------------------
Extra.reduced_sample_collection = 'No'; % Thinned sample collection?
Extra.T = 1000;                         % Every Tth sample is collected
% --------------------------------------------------------------------------------------------
	
% Define the boundary handling
Extra.BoundHandling = 'Reflect';
	
% Save in memory or not
Extra.save_in_memory = 'Yes';
	
% Define feasible parameter space (minimum and maximum values)
%		  1         2        3        4        5         6       7         8
%                 LL        Le       Ss       k1       kr        Sy      Leo       LLo
%ParRange.minn =[1.0E-6,  1.0E-6,  1.0E-8,  1.0E-5,  1.0E-5,  1.0E-3,  1.0E-6,  1.0E-6];
%ParRange.maxn =[1.0E+2,  1.0E+2,  1.0000,  1.0E+3,  1.0E+3,  10.000,  1.0E+2,  1.0E+2];

% parameters 3:5 are log-transformed

ParRange.minn =	[0,    0,  -8,   0,  -6,  0.0,    0,    0];
ParRange.maxn =	[20,  20,   0,  +6,  +6,  0.5,   20,   20];

global idx;

z = load('full-data-and-derivative.csv');

% swap sign on data (no derivative)
Measurement.MeasData = -z(:,1);
Measurement.N = size(Measurement.MeasData,1);

Extra.t = load('full-time.dat');

idx = 1;
	
% Define the boundary handling
Extra.BoundHandling = 'Reflect';

% Define model name
ModelName = 'drive_genslug';
	
% Define option (Model computes output simulation) 
option = 3;

% Indicate the use prior information
%%Extra.InitPopulation = 'PRIOR';
Extra.InitPopulation = 'LHS_BASED';	

%%% Specify the prior distributions for the various parameters

% mean from PEST optimization
% variance from DREAM (interval 5) w/ uninformative prior


%%%      mean         variance
%%P = [3.91256,       0.01998;
%%     5.90970,        0.0004239;
%%     log10(6.135E-5),0.006832;
%%     log10(0.5708),  0.00566;
%%     log10(6.829E-4),0.001192; 
%%     0.4,            0.007821;
%%     9.7712E-2,      0.0001104;
%%     5.09448,        0.3741];
%%
%%Extra.P = P;
%%Extra.prior = {};
%%
%%for i=1:MCMCPar.n
%%    Extra.prior{i} = sprintf('normrnd(%.6g,sqrt(%.6g))',P(i,1),P(i,2));
%%end


% No restart -- just running for the first time. Restart can be used if run
% is termined while running; Use Restart = 'Yes'; to reinitialize the code
% from the saved files. 
Restart = 'No';

more off;
format compact;

% Run the distributed DREAM algorithm with sampling from past
[Sequences,Reduced_Seq,X,Z,output] = dream_zs_parallel(MCMCPar,ParRange,Measurement,ModelName,Extra,option,Restart);

save 'no-downsampling-results.mat' '-V7';
