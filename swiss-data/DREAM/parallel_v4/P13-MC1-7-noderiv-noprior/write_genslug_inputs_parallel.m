function write_genslug_inputs_parallel(Pars,Extra)
%drive_multirate: this script writes input file into directory
%           for parallel execution of oscillating water table slug response

% L_l = Pars(1);     % pumping well factor
% L_e = Pars(2);     % pumping well factor
% S_s = Pars(3);     % formation specific storage
% k_1 = Pars(4);     % skin hydraulic conductivity
% k_r = Pars(5);     % formation radial hydraulic conductivity
% S_y = Pars(6);     % formation specific yield
% Le_o = Pars(7);    % observation well factor
% Ll_o = Pars(8);    % observation well factor

  global idx;

  if Pars(5) > 2*Pars(4)

    % only want a more permeable skin
    save([Extra.dirname, '/DONOTRUN'],'p','-ascii');

  else
    % un-log transform parameters
    p = zeros(8,1);
    p(1:2) = Pars(1:2);  % L and Le not log-transformed
    p(3:5) = 10.0.^Pars(3:5);
    p(6:8) = Pars(6:8);

    save([Extra.dirname,'/parameters.txt'], 'p', '-ascii');
    fprintf('%5i %.3g %.3g %.3g %.3g %.3g %.3g %.3g %.3g\n', ...
                 idx,p(1),p(2),p(3),p(4),p(5),p(6),p(7),p(8));

    idx = idx + 1;
    % don't run program, that is done via mpi script in
    % CompDensity_parallel.m

  end
end
