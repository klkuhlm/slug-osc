#!/bin/bash
#SBATCH --time=48:00:00             # WALLTIME
#SBATCH --account=FY150010          # WC ID
#SBATCH --nodes=8                        # Number of nodes
#SBATCH --partition=ec
#SBATCH --job-name=i7-ndnpR           # Name of job
#SBATCH --mail-type=ALL
#SBATCH --mail-user=klkuhlm@sandia.gov

# Set the job information
set nodes=8
set cores=8

# switch to modern gcc compilers
module unload intel
module load gnu/4.7.2

# switch to openmpi compatible with gcc
module unload openmpi-intel
module load openmpi-gnu/1.6

octave runDREAM_ZS_p13mc1i7.m
