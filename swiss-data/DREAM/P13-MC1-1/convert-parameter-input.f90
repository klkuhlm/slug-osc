program convert
  implicit none
  integer, parameter :: DP = 8
  real(DP), dimension(8) :: x
  character(14), dimension(8), parameter :: c = ['            ll', & 
       & '            le', & 
       & '            ss', & 
       & '            k1', & 
       & '            kr', & 
       & '            sy', & 
       & '           leo', & 
       & '           llo']
  integer :: i

  open(unit=22,file='parameters.txt',action='read',status='old')

  read(22,*) x
  close(22)
  
  open(unit=33,file='test1obs.par',action='write',status='replace')
  write(33,'(A)') '  double point'

!!$  do i=1,5
!!$     write(33,'(A,2X,ES22.14,8X,2(F8.6,9X))') c(i),x(i),1.0,0.0
!!$  end do
!!$  ! specific yield (6th in par list) is fixed
!!$  write(33,'(A,2X,ES22.14,8X,2(F8.6,9X))') c(6),3.5D-1,1.0,0.0
!!$  do i=6,7
!!$     write(33,'(A,2X,ES22.14,8X,2(F8.6,9X))') c(i+1),x(i),1.0,0.0
!!$  end do
  do i=1,8
     write(33,'(A,2X,ES22.14,8X,2(F8.6,9X))') c(i),x(i),1.0,0.0
  end do
  

  close(33)

end program convert
