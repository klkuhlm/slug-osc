#!/bin/bash

# cleanup old parameter file
rm -f test1obs.par

# variables have been saved into file by matlab
# convert file to PEST parameter file format to use pest utilities
./convert

# run PEST utility tempchek to generate model input from tempate and parameter file
tempchek test1obs.ptf input-gen.dat test1obs.par > tempchek.dbg

# set to allow using dynamic linking with trunk gfortran compiler
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/klkuhlm/local/lib64

# threads to use by OpenMP program
export OMP_THREAD_LIMIT=8
export OMP_NUM_THREADS=8

# cleanup old output file
rm -f test1obs.out

./genslug

# run PEST utility inschek to extract output from model-generated file
#inschek test1obs.ins test1obs.out > inschek.dbg

# delete header (first 18 rows)
sed -i '1,18d' test1obs.out

# cleanup old final output file
#rm -f test1obs.obf.data

# data is second column, extract it from results of inschek
#awk '{print $2}' test1obs.obf > test1obs.obf.data
