
clear;

load 'results-Jan16-5chain-deriv-prior.mat';
%load 'DREAM_ZS.mat';

prefix = 'Jan16-prior';

%%      mean         variance
%P = [3.91256,       0.01998;
%    5.90970,        0.0004239;
%    log10(6.135E-5),0.006832;
%    log10(0.5708),  0.00566;
%    log10(6.829E-4),0.001192; 
%    0.4,            0.007821;
%    9.7712E-2,      0.0001104;
%    5.09448,        0.3741];

Extra.P = P;

nchains = size(Sequences,3);
npar = size(Sequences,2)-2;
niter = 15000; %size(Sequences,1);

vars = {'L_L','L_e','log_{10}(S_s)','log_{10}(K_1)', ...
        'log_{10}(K_r)','S_y','L_{eo}','L_{Lo}'};

colors = {'r','g','b','c','m','k','y','r'};

% plot of chains
% ***************
if 0
    figure();
    for i = 1:npar
        subplot(npar,1,i);
        % plot each chain as a differnt colors
        for j =1:nchains
            plot(Sequences(:,i,j),[colors{j} '-']);
            hold on;
        end
        ylabel(vars{i},'FontSize',12);
        xlim([0,niter]);
    end

    xlabel('iterations per chain','FontSize',9);
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-all-chains.eps']);
end
    
% reshape output after burnin period
burnin = 200;
jump = niter-burnin;
tmp = zeros(npar+1,jump*nchains,1);
for i = 1:npar

    for j=1:nchains
        tmp(i,jump*(j-1)+1:j*jump) = Sequences(burnin+1:niter,i,j);
    end
end

tmp(tmp == 0.0) = NaN;

% plot of histograms
% ******************

nbins = 25;

if 1
    figure();
    for i = 1:npar
        subplot(4,2,i);
        % normalized histogram  from 
        % http://www.mathworks.com/matlabcentral/fileexchange/
        % 22802-normalized-histogram
        xx = tmp(i,:);
        [n,xout] = histnorm(xx,nbins);
        m = mean(xx(~isnan(xx)));
        v = var(xx(~isnan(xx)));
        bar(xout,n,'BarWidth',1.0);
        hold on;
        xlabel(vars{i},'FontSize',12);
        ylabel('Posterior density','FontSize',9);
        
        % normal dist
        plot(xout,exp(-(xout-m).^2 ./(2*v))./sqrt(2*pi*v),'r-');
        
        mm = Extra.P(i,1);
        vv = Extra.P(i,2);
        
        plot(xout,exp(-(xout-mm).^2 ./(2*vv))./sqrt(2*pi*vv),'g-');
        
        fmtstr = sprintf('%.4g',m);
        text(0.1,0.8,['\mu_{post}=',fmtstr],'Units','normalized','color','red');
        
        fmtstr = sprintf('%.4g',mm);
        text(0.1,0.6,['\mu_{prior}=',fmtstr],'Units','normalized','color','green');
        
        fmtstr = sprintf('%.4g',v);
        text(0.7,0.8,['\sigma^{2}_{post}=',fmtstr],'Units','normalized','color','red');
        
        fmtstr = sprintf('%.4g',vv);
        text(0.7,0.6,['\sigma^{2}_{prior}=',fmtstr],'Units','normalized','color','green');
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-posterior-histograms.eps']);
end

% plot of correlation between posterior distributions
% ***************************************************
if 0
    figure();
    [H,AX,BigAx,P,PAx] = plotmatrix(transpose(tmp(1:npar,:)),'r*');
    for i = 1:npar
       title(BigAx,'Pariwise distributions of DREAM results')
        ylabel(AX(i,1),vars{i},'FontSize',9);
        xlabel(AX(npar,i),vars{i},'FontSize',9);
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-posterior-joint-distributions.eps']);
end

% horsetail plot (must be re-computed and takes a while)
% *******************************************************
if 0
    nhorsetail = 100;

    times = linspace(min(Extra.t),max(Extra.t),50)';
    Struct.times = times;

    nt = size(times,1);
    NT = size(Extra.t,1);

    figure();
    for i = 1:nhorsetail
        for j = 1:nchains
            if mod(i,10) == 0 && j == 1
                disp(nchains*i);
            end

            S = drive_genslug(Sequences(niter-i,:,j),Struct);

            subplot(121);
            plot(times,S(1:nt),'k-');
            hold on;
            subplot(122);
            plot(times,S(nt+1:end),'k-');
            hold on;
        end
    end
    subplot(121);
    plot(Extra.t,Measurement.MeasData(1:NT),'ro');
    xlabel('time [seconds]');
    ylabel('Head [m]');
    grid on;
    xlim([0,17]);
    ylim([-0.005,0.005]);

    subplot(122);
    plot(Extra.t,Measurement.MeasData(NT+1:end),'ro');
    xlabel('time [seconds]');
    ylabel('\partial Head / \partial log(t) [m]');
    grid on;
    xlim([0,17]);
    ylim([-0.015,0.015]);
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-horsetail.eps']);
    
end

