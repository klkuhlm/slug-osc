function [p,log_p] = CompDensity_scaled_parallel(x,MCMCPar,Measurement,ModelName,Extra,option)
% This function computes the density of each x value

p = []; 
log_p = [];

% assumes there are a number of directories named 01, 02, 03, ...., N
% (N = total number of chains), where each model run is executed in parallel

% step 1: write input files for N chains into run directories
% step 2: call bash script that executes on N nodes using mpiexec
%         while each forward run is using 8 cores on each node
%         code writes empty file with ".DONE" appended to input name when finished
% step 3: loop through directories, checking for "DONE" file and
%         loading results
% 

% wait this many "cycles" of 0.1 seconds each
maxcount = 1200;

% step 1: write inputs into each directory
for ii = 1:size(x,1)
    Extra.dirname = sprintf('%2.2i',ii);
    write_genslug_inputs_scaled_parallel(x(ii,:),Extra);
end
    
% step 2: spawn off N versions of genslug and exit
unix('./run_genslug.sh');

% Loop over the individual parameter combinations of x
% reading in output files from directories
count = 0;
for ii = 1:size(x,1)

    d = sprintf('%2.2i',ii); % zero-padded directory

    outfile  = [d,'/test1obs.out'];
    donefile = [d,'/input-gen.dat.DONE'];

    % wait for DONE file (share waiting time between all threads)
    while ~exist(donefile,'file')
        pause(0.1); 
        count = count + 1;
        if count > maxcount
           % got tired of waiting for this
           break;
        end
    end

    % strip header off output file (not necessary in octave?)
    unix(['sed -i -e "s/NaN/99999/g" -e "s/Inf/99999/g" ',outfile]);
    unix(['sed -i "1,18d" ',outfile]);

    % load output file
    if count < maxcount
        z = load(outfile);
        ModPred = z(:,2);  % _NO_ derivative
    else
        % code hung up and was terminated early
        ModPred = 999999999.9*ones(size(Measurement.MeasData));		
    end

    % assumes "option == 3"
    Err = (Measurement.MeasData(:)-ModPred(:));
    % Derive the sum of squared error
    SSR = sum(Err.^2);
    % And retain in memory
    p(ii,1) = -SSR; 
    log_p(ii,1) = -0.5 * SSR;

end

