#!/bin/bash

# substitute MATLAB variables in "parameters.txt" into input file for genslug
python ./replace_tempchek_horsetail.py

# remove output file before re-running
rm -f ./test1obs.out

# matlab resets this to point to its libraries (thanks a heap)
export LD_LIBRARY_PATH=''
export OMP_NUM_THREADS=18

# call genslug to do simulation
./genslug input-gen.dat 2>&1 > genslug.dbg

# strip off header of output, to simplify MATLAB import of results
#sed -i "1,18d" ./test1obs.out
