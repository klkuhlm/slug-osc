
set(gcf, 'visible','off');
clear;

psize = [7,5];
psize_sm = [3,2];

plot_pest_results = 1;
tshift = 0.445;
deriv = 0;

pest_results =  [2.82025	3.18471	1.082482E-005	0.0773497	0.000804269	0.4	0.01756298	3.09956];

hist_bounds = [0.0, 10.0;
               0.0, 4.0;
               -8.0, -4.0;
               -3.0, -0.5;
               -3.4, -2.6;
               0.0, 0.4;
               0.0, 6.5;
               0.0, 14.0];

% pest_results has 8 cols (8 parameters: ll, le, ss, k1, kr, sy, leo, llo)

load 'DREAM_ZS.mat';

prefix = 'P13MC15-finite-fixed-shift2';

% log transform PEST results
pest_results(3:5) = log10(pest_results(3:5));

% paramter 4 (k1) is a multiplier on parameter 5 (kr)
Sequences(:,4:5,:) = 10.0.^Sequences(:,4:5,:);
Sequences(:,4,:) = Sequences(:,4,:).*Sequences(:,5,:);

% parameter 4 (K1) is K_skin (same skin on signal & monitoring wells: K1=K3)

Sequences(:,4:5,:) = log10(Sequences(:,4:5,:));                      
                                             
nchains = size(Sequences,3);
npar = size(Sequences,2)-2;
niter = 22300;
%niter = size(Sequences,1);

vars = {'L','L_e','log_{10}(S_s)','log_{10}(K_{skin})', ...
        'log_{10}(K)','S_y','L_{e,obs}','L_{obs}'};

colors = {'r','g','b','c','m','k','y','r'};

% plot of chains
% ***************
if 0
    figure();
    for i = 1:npar
        subplot(npar,1,i);
        % plot each chain as a differnt colors
        for j =1:nchains
            plot(Sequences(:,i,j),[colors{j} '-']);
            hold on;
        end
        ylabel(vars{i},'FontSize',12);
        xlim([0,niter]);
    end

    xlabel('iterations per chain','FontSize',9);
    
    %set(gcf,'PaperType','usletter');
    set(gcf,'PaperUnits','inches');
    set(gcf,'PaperSize',psize);
    set(gcf,'PaperOrientation','landscape');   
    print('-depsc2',[prefix,'-all-chains.eps']);
end
    
% reshape output after burnin period
burnin = 10000;
jump = niter-burnin;
tmp = zeros(npar+1,jump*nchains,1);
for i = 1:npar
    for j=1:nchains
        tmp(i,jump*(j-1)+1:j*jump) = Sequences(burnin+1:niter,i,j);
    end
end

Le_gt2_mask = tmp(2,:) > 2.0;
Leo_gt2_mask = tmp(7,:) > 2.0;

tmp(tmp == 0.0) = NaN;

% plot of histograms
% ******************

nbins = 25;

if 1
    figure();
    for i = 1:npar
        subplot(4,2,i);
        % normalized histogram  from 
        % http://www.mathworks.com/matlabcentral/fileexchange/22802-normalized-histogram
        xx = tmp(i,Le_gt2_mask);
        [n,xout] = histnorm(xx,nbins);
        m = mean(xx(~isnan(xx)));
        v = var(xx(~isnan(xx)));
        hbarb = bar(xout,n,'b','BarWidth',1.0);
        hPatch1 = findobj(hbarb,'Type','patch');
        hh1 = hatchfill(hPatch1,'single',-45,1.25);
        set(hh1,'Color','blue');
        hold on;
        
        xx = tmp(i,~Le_gt2_mask);
        [n,xout] = histnorm(xx,nbins);
        m = mean(xx(~isnan(xx)));
        v = var(xx(~isnan(xx)));
        hbarr = bar(xout,n,'r','BarWidth',1.0);
        hPatch2 = findobj(hbarr, 'Type','patch');
        hh2 = hatchfill(hPatch2,'single',45,1.25);
        set(hh2,'Color','red')
               
        xlabel(vars{i},'FontSize',12);
        ylabel('Posterior density','FontSize',9);
        
        yl = ylim();
        %xl = xlim();
        
        % red vertical line showing PEST estimate
        hline = plot(pest_results([i,i]),[0,1.0E+6], 'LineStyle','-', 'Color',[1 0 0]);
        
        % reset scales
        ylim([0,yl(2)]);
        xlim(hist_bounds(i,:));
  
    end
    
    %set(gcf,'PaperType','usletter');
    set(gcf,'PaperUnits','inches');
    set(gcf,'PaperSize',psize_sm);
    set(gcf,'PaperOrientation','landscape');       
    print('-depsc2',[prefix,'-posterior-histograms.eps']);
end

% plot of correlation between posterior distributions
% ***************************************************
if 1
    figure();
    [H,AX,BigAx,P,PAx] = plotmatrix(transpose(tmp(1:npar,Le_gt2_mask)),transpose(tmp(1:npar,Le_gt2_mask)),'b.');
    %title(BigAx,'Pariwise distributions of DREAM results')
    for i = 1:npar
        
        ylabel(AX(i,1),vars{i},'FontSize',9);
        xlabel(AX(npar,i),vars{i},'FontSize',9);
        for j = 1:npar
            if j > i
                %size(tmp(i,~Le_gt2_mask))
                %size(tmp(j,~Le_gt2_mask))
                hold on;
                scatter(AX(i,j),tmp(i,~Le_gt2_mask),tmp(j,~Le_gt2_mask),'r.');
            end
            ylim(AX(i,j),hist_bounds(i,:));
            xlim(AX(i,j),hist_bounds(j,:));
        end
    end
    
    %set(gcf,'PaperType','usletter');
    set(gcf,'PaperUnits','inches');
    set(gcf,'PaperSize',psize);
    set(gcf,'PaperOrientation','landscape');
    print('-depsc2',[prefix,'-posterior-joint-distributions.eps']);
end

% horsetail plot (must be re-computed and takes a while)
% *******************************************************
if 0
    nhorsetail = 10;  % re-run model nhorsetail*nchains times

    nt = 75;  % this must be same as that in python script
    
    times = linspace(0.0,(max(Extra.t) + tshift)*1.05,75)' + 1.0E-4;
    Struct.times = times;

    save('./horsetail75-time.dat','times','-ascii');
    
    if deriv == 0
        z = load('downsampled5-data-and-derivative.csv');

        % DO NOT swap sign on data (and its derivative)
        Measurement.MeasData = [z(:,1);z(:,2)];
        
        Extra.t = load('downsampled5-time.dat');
        
    end
    
    NT = size(Extra.t,1);

    p = zeros(8,1);
    
    figure();
    for i = 1:nhorsetail
        for j = 1:nchains
            disp((i-1)*nchains + j);
            
            % write parameters into form expected by genslug
            p(1:8) = Sequences(niter-i,1:8,j);  % L and Le not log-transformed
            p(3:5) = 10.0.^p(3:5);

            save('parameters.txt', 'p', '-ascii');
            unix('python ./replace_tempchek_horsetail.py');
            unix('./matlab_genslug.sh');
            unix('sed -i "1,18d" ./test1obs.out');

            z = load('test1obs.out');
            S = [z(:,2);z(:,3)];

            %subplot(121);
            plot(times,S(1:nt),'k-');
            hold on;
%             subplot(122);
%             plot(times,S(nt+1:end),'k-');
%             hold on;
        end
    end
    
    if plot_pest_results
        
        % write parameters into form expected by genslug               
        disp('PEST');
        p = zeros(8,1);
        p(1:2) = pest_results(1:2);
        p(3:5) = 10.0.^pest_results(3:5);
        p(6:8) = pest_results(6:8);
        
        save('parameters.txt', 'p', '-ascii');
        unix('./matlab_genslug.sh');
        unix('sed -i "1,18d" ./test1obs.out');

        z = load('test1obs.out');
        S = [z(:,2);z(:,3)];
        
        %subplot(121);
        plot(times,z(:,2),'r-','linewidth',3);
        hold on;
%         subplot(122);
%         plot(times,z(:,3),'c--');
%         hold on;
    end

    
%     subplot(121);
    plot(Extra.t + tshift,Measurement.MeasData(1:NT),'b-');
    xlabel('Time (s)');
    ylabel('Observation well response, s_D');
    grid on;
    xlim([0,17]);
    ylim([-0.004,0.007]);

%     subplot(122);
%     if deriv
%         plot(Extra.t + tshift,Measurement.MeasData(NT+1:end),'ro');
%     else
%         plot(Extra.t + tshift,Measurement.MeasData(NT+1:end),'gx');
%     end
%     xlabel('time [seconds]');
%     ylabel('\partial Head / \partial log(t) [m]');
%     grid on;
%     xlim([0,17]);
%     ylim([-0.015,0.015]);
            
    %set(gcf,'PaperType','usletter');
    set(gcf,'PaperUnits','inches');
    set(gcf,'PaperSize',psize_sm);
    set(gcf,'PaperOrientation','landscape');   
    print('-depsc2',[prefix,'-horsetail.eps']);
    
end
