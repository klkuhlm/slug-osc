import numpy as np

# downsampled
deriv = np.loadtxt('obs_well-P13-MC1-3.txt')

# not downsampled
orig = np.loadtxt('obs_well-P13-MC1-3.dat',skiprows=1,delimiter=',')

fh = open('downsampled5-data-and-derivative.csv','w')

for j in range(deriv.shape[0]):
    tt = deriv[j,0]

    for k in range(orig.shape[0]):
        if abs(orig[k,0] - tt) < 1.0E-5:
            fh.write('%.6g,%.6g\n' % (orig[k,1],deriv[j,1]))


fh.close()
