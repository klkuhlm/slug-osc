#!/bin/bash
#SBATCH --time=36:00:00             # WALLTIME
#SBATCH --account=FY150010          # WC ID
#SBATCH -N 8                        # Number of nodes
#SBATCH --job-name parallel-prior           # Name of job
#SBATCH --mail-type=ALL
#SBATCH --mail-user=klkuhlm@sandia.gov

# Set the job information
set nodes=8
set cores=8

# switch to modern gcc compilers
module unload intel/12.1
module load gnu/4.7.1

# switch to openmpi compatible with gcc
module unload openmpi-intel/1.6
module load openmpi-gnu/1.6

# load BLAS/LAPACK compatible with octave
module load atlas


${HOME}/local/bin/octave runDREAM_ZS_p13mc1i3_scaled_prior.m
