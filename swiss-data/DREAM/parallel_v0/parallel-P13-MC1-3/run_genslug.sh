#!/bin/bash 

# switch to modern gcc compilers
module unload intel/12.1
module load gnu/4.7.1

# switch to openmpi compatible with gcc
module unload openmpi-intel/1.6
module load openmpi-gnu/1.6

# load BLAS/LAPACK compatible with octave
module load atlas

for i in {1..8}; do

    DIR=$(printf '%2.2i' ${i}) # two-digit directories
    NODE=$((i-1))  # these are zero-based

    cd ${DIR}

    # if invalid inputs, a "DONOTRUN" file is created

    # write input file for genslug with parameter values in 
    # parameters.txt substituted into the input file
    python replace_tempchek.py

    # look for DONE file to see if done, delete old version
    rm -f input-gen.dat.DONE test1obs.out
      
    # start genslug on a relative node, using all 8 cores
    # (code uses OpenMP for parallelization)
    # fork new process, don't wait for results
    mpiexec -n 1 -host +n${NODE} ./genslug &

    cd ..
done
