
set(gcf, 'visible','off');
clear;

deriv = 0;

pest_results =  ...
    [1.788945E-04      -1.070376E-03      1.428165E-03; ...
     0.533971           -1.59493           2.66287; ...
     1.637652E-03      -3.398944E-03      6.674248E-03; ...
     0.400000           -3.98295           4.78295;];

% variance values extracted from diagonal of PEST covariance matrix (from .rec file)
% these are variances of linear variables, not log-transformed ones
pest_var = [4.0626E-07, 1.180,  6.6033E-06, 5.001 ];

% pest_results has 4 rows (4 parameters: ss, k1, kr, sy)
% and three columns (estimated, 95% lower, 95% upper)

% pest results are all linear (no log-transformed variables)

% 8 parameters (PEST before cutting, original DREAM)
% 1   2   3   4   5   6   7    8
% ll, le, ss, k1, kr, sy, leo, llo

% 4 parameters (new DREAM)
% 1   2   3   4
% ss, k1, kr, sy

% parameters 1-3 in DREAM results are log-transformed (Sy is linear)

load 'DREAM_ZS.mat';

prefix = 'P13MC11-4param';

% in DREAM paramter 2 (k1) is a multiplier on parameter 3 (kr)
Sequences(:,2:3,:) = 10.0.^Sequences(:,2:3,:);
Sequences(:,2,:) = Sequences(:,2,:).*Sequences(:,3,:);

% parameter 2 (K1) is K_skin (same skin on signal & monitoring wells: K1=K3)

Sequences(:,2:3,:) = log10(Sequences(:,2:3,:));         
                                             
nchains = size(Sequences,3);
npar = size(Sequences,2)-2;
niter = 15500;
%niter = size(Sequences,1);

vars = {'log_{10}(S_s)', 'log_{10}(K_{1})', 'log_{10}(K_r)', 'S_y'};

colors = {'r','g','b','c','m','k','y','r'};

% plot of chains
% ***************
if 0
    figure();
    for i = 1:npar
        subplot(npar,1,i);
        % plot each chain as a differnt colors
        for j =1:nchains
            plot(Sequences(:,i,j),[colors{j} '-']);
            hold on;
        end
        ylabel(vars{i},'FontSize',12);
        xlim([0,niter]);
    end

    xlabel('iterations per chain','FontSize',9);
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-all-chains.eps']);
end
    
% reshape output after burnin period
burnin = 5000;
jump = niter-burnin;
tmp = zeros(npar+1,jump*nchains,1);
for i = 1:npar
    for j=1:nchains
        tmp(i,jump*(j-1)+1:j*jump) = Sequences(burnin+1:niter,i,j);
    end
end

tmp(tmp == 0.0) = NaN;

% plot of histograms
% ******************

nbins = 25;

if 0
    figure();
    for i = 1:npar
        subplot(2,2,i);
        % normalized histogram  from 
        % http://www.mathworks.com/matlabcentral/fileexchange/22802-normalized-histogram
        xx = tmp(i,:);
        [n,xout] = histnorm(xx,nbins);
        hbar = bar(xout,n,'BarWidth',1.0);
        hold on;
        xlabel(vars{i},'FontSize',12);
        ylabel('Posterior density','FontSize',9);
        
        yl = ylim();

        % plot a vertical red line for pest estimate
        if i < 4
            % log-transform K's and Ss
            plot(log10(pest_results(i,[1,1])),yl,'r-');
            xl = xlim();
            xp = 10.0.^linspace(xl(1),xl(2),75);
        else
            % not log-transformed Sy
            plot(pest_results(i,[1,1]),yl,'r-');
            xl = xlim();
            xp = linspace(xl(1),xl(2),75);
        end
        
        mp = pest_results(i,1);
        vp = pest_var(i);
        
%         if i < 4
%             % normal dist from PEST results, plotted in log-space
%             plot(log10(xp),exp(-((xp-mp).^2)./(2*vp))./sqrt(2*pi*vp),'g-');
%         else
%             plot(xp,exp(-((xp-mp).^2)./(2*vp))./sqrt(2*pi*vp),'g-');
%         end
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-dpng',[prefix,'-posterior-histograms.png']);
end

% plot of correlation between posterior distributions
% ***************************************************
if 0
    figure();
    [H,AX,BigAx,P,PAx] = plotmatrix(transpose(tmp(1:npar,:)),'r.');
    for i = 1:npar
       title(BigAx,'Pariwise distributions of DREAM results')
        ylabel(AX(i,1),vars{i},'FontSize',9);
        xlabel(AX(npar,i),vars{i},'FontSize',9);
    end
    
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-posterior-joint-distributions.eps']);
end

% horsetail plot (must be re-computed and takes a while)
% *******************************************************
if 1
    nhorsetail = 1;  % re-run model nhorsetail*nchains times

    nt = 75;  % this must be same as that in python script
    times = linspace(min(Extra.t),max(Extra.t),75)';
    Struct.times = times;

    if deriv == 0
        z = load('full-data-and-derivative.csv');

        % swap sign on data (and its derivative)
        Measurement.MeasData = [-z(:,1);-z(:,2)];
    end

    NT = size(Extra.t,1);
    p = zeros(4,1);
    
    % plot the PEST optimum as a different colored line in horsetail plot?
    plot_pest_results = 1;
    
    figure();
    for i = 1:nhorsetail
        for j = 1:nchains
            disp((i-1)*nchains + j);
            
            % write parameters into form expected by genslug
            p(1:4) = Sequences(niter-i,1:4,j);  
            p(1:3) = 10.0.^p(1:3);
            
            save('parameters.txt', 'p', '-ascii');
            unix('./matlab_genslug.sh');

            z = importdata('test1obs.out',' ',18);
            z = z.data;
            
            subplot(121);
            plot(z(:,1),z(:,2),'k-');
            hold on;
            subplot(122);
            plot(z(:,1),z(:,3),'k-');
            hold on;
        end
    end
    
    if plot_pest_results
        
        % write parameters into form expected by genslug
        p(1:4) = pest_results(1:4,1);    % ss, k1, kr, Sy
                
        disp('PEST');
        
        save('parameters.txt', 'p', '-ascii');
        unix('./matlab_genslug.sh');

        z = importdata('test1obs.out',' ',18);
        z = z.data;
                    
        subplot(121);
        plot(z(:,1),z(:,2),'c--');
        hold on;
        subplot(122);
        plot(z(:,1),z(:,3),'c--');
        hold on;
    end

    
    subplot(121);
    plot(Extra.t,Measurement.MeasData(1:NT),'ro');
    xlabel('time [seconds]');
    ylabel('Head [m]');
    grid on;
    xlim([0,17]);
    ylim([-0.005,0.005]);

    subplot(122);
    if deriv
        plot(Extra.t,Measurement.MeasData(NT+1:end),'ro');
    else
        plot(Extra.t,Measurement.MeasData(NT+1:end),'gx');
    end
    xlabel('time [seconds]');
    ylabel('\partial Head / \partial log(t) [m]');
    grid on;
    xlim([0,17]);
    ylim([-0.015,0.015]);
            
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-horsetail.eps']);
    
end
