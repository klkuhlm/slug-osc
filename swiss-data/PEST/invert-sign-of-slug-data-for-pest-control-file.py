
dirs = ['finite-P13-MC1-9']
dirs.extend(['finite-P13-MC2-%2.2i' % i for i in [2,4,6,8,10]])
dirs.extend(['finite-P13-MC4-%i' % i for i in [2,4,6,8]])

# only run this ONCE! or it will swap sign back (doesn't back original file up)

for d in dirs:
    print d
    fname = d + '/test1obs.pst'
    fhin = open(fname,'r')
    lines = fhin.readlines()
    fhin.close()

    data_begin = -999

    ndata = int(lines[3].split()[1])
    
    for i,line in enumerate(lines):
        if '* observation data' in line:
            data_begin = i+1
            break
        
    data_end = data_begin + ndata
        
    fhout = open(fname,'w')

    # echo section before "*observation data" verbatim
    for line in lines[:data_begin]:
        fhout.write(line)

    for line in lines[data_begin:data_end]:
        f = line.split()
        f[1] = str(-float(f[1]))
        fhout.write(' '.join(f) + '\n')

    # echo section from "*model command line" to end verbatim
    for line in lines[data_end:]:
        fhout.write(line)
    
