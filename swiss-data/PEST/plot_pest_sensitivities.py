import numpy as np
import matplotlib.pyplot as plt
#from glob import glob

fig = plt.figure(1,figsize=(17,17))
axv = {}

parameters = ['ll','le','ss','k1','kr','kz','sy','leo','llo']

for i,par in enumerate(parameters):
    axv[par] = fig.add_subplot(3,3,i+1)

#for d in ['P13-MC1-%i'%x for x in [1,3,5,7,9]]:
#for d in ['P13-MC2-%2.2i'%x for x in [2,4,6,8]]:
for  d in ['P13-MC4-%i'%x for x in [2,4,6,8]]:
    
    interval = d.split('-')[2]

    data = {}

    fn = d+'/test1obs_jco.txt'
    fh = open(fn,'r')
    headers = [x.strip().lower() for x in fh.readline().split()]
    fh.close()

    for j,h in enumerate(headers):
        data[h] = np.loadtxt(fn,skiprows=2,usecols=(j+1,))

    print d,headers,data[data.keys()[0]].shape

    t = np.loadtxt(d+'/times.dat')
    nn = min(t.shape[0],data[data.keys()[0]].shape[0])

    for par in data.keys():
        axv[par].plot(t[:nn],data[par][:nn],label='int %s' % interval)
        axv[par].set_xlabel('time (sec)')
        axv[par].set_ylabel('sensitivities')
        axv[par].set_title(par)

for par in parameters:
    axv[par].legend(loc=0,prop={'size':8},ncol=3)
    axv[par].grid()

plt.suptitle(' '.join(d.split('-')[0:2]))
plt.tight_layout()
plt.savefig('sensitivities-%s-%s.pdf' % tuple(d.split('-')[0:2]))
plt.close(1)
        
