import numpy as np
import subprocess
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

dirs = ['finite-P13-MC1-%i' % j for j in [1,3,5,7,9]]
dirs.extend(['finite-P13-MC2-%2.2i' % j for j in [2,4,6,8,10]])
dirs.extend(['finite-P13-MC4-%i' % j for j in [2,4,6,8]])

results_dir = 'finite-PEST-timeshift-noObsSkin'

results = []
out = []
pars = []
cvar = []
var = []

npars = []
nobs = []

timeshift = -999.9

for i,d in enumerate(dirs):
    print i,d
    
    fh = open('%s/test1obs.rec' % d,'r')
    lines = fh.readlines()
    fh.close()

    numtotalpar = -999
    numpar = -999
    numobs = -999
    for line in lines:
        if "Number of parameters" in line:
            numtotalpar = int(line.strip().split()[-1]) # includes fixed parameter timeshift
        elif "Number of adjustable parameters" in line:
            numpar = int(line.strip().split()[-1])
        elif "Number of observations" in line:
            numobs = int(line.strip().split()[-1])
            break
    npars.append(numpar)
    nobs.append(numobs)
    idx = -999

    if numtotalpar > numpar:
        for line in lines:
            if "tshift       fixed" in line:
                timeshift = float(line.split()[3])
    else:
        print 'TIMESHIFT ESTIMATED FROM DATA FOR ',d
                
    
    for j,line in enumerate(lines):
        # find block of opimization results
        if "OPTIMISATION RESULTS" in line:
            if "Covariance matrix and parameter confidence intervals" not in lines[j+2]:
                idx = j + 7
                Covariance_Matrix = True
            else:
                idx = j + 9
                Covariance_Matrix = False
            break
    
    results.append({})
    pars.append([])
    for line in lines[idx:idx+numpar]:
        f = line.strip().split()
        results[i][f[0]] = float(f[1])
        pars[i].append(f[0])
            
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    idx = -999
    for j,line in enumerate(lines):
        # find block of measured/modeled values at optimum
        if " Observations ----->" in line:
            idx = j + 4
    
    out_rzn = []
    for line in lines[idx:idx+numobs]:
        f = line.strip().split()
        out_rzn.append([float(x) for x in f[1:4]])
    
    out.append(np.array(out_rzn))
    t = np.loadtxt('%s/times.orig' % d)
    tfinal = t.max()*1.05
    
    # re-run slug model with uniform data -> 0
    nfinal = 300
    tt = np.linspace(0, tfinal, nfinal) + 1.0E-4
    np.savetxt('%s/times-final.dat' % d, tt)
    fhin = open('%s/input-finite.dat' % d,'r')
    inlines = fhin.readlines()
    fhin.close()
    
    fhout = open('%s/input-finite-final.dat' % d,'w')

    # turn on echo to screen
    inlines[0] = 'F  F  F  F  16.0 \n'
    # increase number of fourier / hankel terms
    inlines[-4] = '35  1.0D-12  1.0D-13  1500 \n'
    # re-write line with number of times and time file
    inlines[-2] = 'F  %i times-final.dat \n' % nfinal
    # change output filename
    inlines[-1] = 'test1obs-final.out \n'
    for line in inlines:
        fhout.write(line)
    fhout.close()

    path = '/home/klkuhlm/repos/slug-osc/swiss-data/PEST/%s/' %d
    p = subprocess.Popen(path + 'genslug_finite ./input-finite-final.dat', cwd=d,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    print 'running with final data',d
    stdout,stderr = p.communicate()
    #print stdout
    print stderr
    
    zz = np.loadtxt('%s/test1obs-final.out' % d)
    
    fig = plt.figure(1,figsize=(6,5))
    ax = fig.add_subplot(111)
    print 't.shape; out.shape',t.shape,out[i][:,0].shape
    ax.plot(t+results[i].get('tshift',timeshift),out[i][:,0]*100,'b,')
    ax.axhline(0,color='black',linewidth=0.75)
    ax.plot(t+results[i].get('tshift',timeshift),out[i][:,0]*100,'b-',linewidth=0.5)
    ax.plot(zz[:,0],zz[:,1]*100,'r-')
    ax.set_xlim([0,tfinal])
    ax.set_xlabel('time (s)')
    ax.set_ylabel('Observation well response, $s_D$ ($\\times 10^{2}$)')
    #ax.set_title(d.lstrip('finite-'))
    ax.annotate(d.lstrip('finite-'),xy=(0.75,0.95),xycoords='axes fraction')
    #ax.grid()
    plt.tight_layout()
    plt.savefig('%s/%s-model_pest_fit.pdf' % (results_dir,d))
    plt.close()
    
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    idx = -999
    if Covariance_Matrix:
        for j,line in enumerate(lines):
            # find covariance matrix
            if "Parameter covariance matrix ----->" in line:
                idx = j + 2
        
        cvar_rzn = []
        print 'numpar',numpar
        if numpar <= 8:
            # one row of column headers, then matrix starts next row
            for line in lines[idx+1:idx+1+numpar]:
                f = line.strip().split()
                cvar_rzn.append([float(x) for x in f[1:numpar+1]])
        elif numpar <= 16:
            # two rows of column headers, then empty row before matrix starts
            # empty row in .rec file between wrapped rows of covar matrix

            # PEST wraps covariance matrix at 8 columns
            for line in lines[idx+3:idx+3+(numpar*3):3]:
                f = line.strip().split()
                cvar_rzn.append([float(x) for x in f[1:numpar+1]])
            
            for ii,line in enumerate(lines[idx+4:idx+3+(numpar*3):3]):
                f = [float(x) for x in line.strip().split()]
                cvar_rzn[ii].extend(f)
                
        else:
            print 'ERROR: code to read covariance matrix not general enough'
            print '  to handle large number of parameters yet'
            print '  either reformat output or fix this.'

        cvar_rzn = np.array(cvar_rzn)
        print 'cvar_rzn shape',cvar_rzn.shape
        cvar.append(cvar_rzn)
        var.append({})
        print 'pars[i]',i,len(pars)
        print 'cvar[i]',cvar[i].shape
        for j,par in enumerate(pars[i]):
            # get variance (diagonal of covariance matrix)
            # save into dictionary
            var[i][par] = cvar[i][j,j]
    else:
        cvar.append(np.zeros((1,1)))
        var.append({})

    
    
# master list of columns
cols = pars[0]
        
csvfh = open('%s/results.csv' % results_dir,'w')
csvfh.write('interval,%s,%s,number observations,number parameters\n' %
            (','.join(['estimate %s'%s for s in cols]),
             ','.join(['variance %s'%s for s in cols])))
for i,d in enumerate(dirs):
    csvfh.write(d+',')
    # handle when Sy is not estimated
    csvfh.write(','.join(['%s' % results[i].get(s,'NE') for s in cols])+',')
    csvfh.write(','.join(['%s' %     var[i].get(s,'NE') for s in cols])+',')
    csvfh.write('%i,%i\n' % (nobs[i],npars[i]))
    
csvfh.close()
    
            
