from scipy.interpolate import UnivariateSpline as spline
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from glob import glob
from numpy.random import choice

headweight = 1.0
derivweight = 0.4

for df in glob('obs_well-P13-MC1-?.dat'):
    
    print 'loading:',df
    tmin = 0.0
    
    t,p = np.loadtxt(df,delimiter=',',unpack=True,skiprows=1)
    
    m = t > tmin
    tt = t[m]
    ppp = p[m]

    # sampling "period"
    sT = 1.0  # non-integer values only make sense for non-uniform sampling
    sample_uniform = False

    if sample_uniform:
        tp = tt[::sT]  # downsample uniformly
        pp = ppp[::sT]
    else:
        # sample one random value from each "period" (latin hypercube)
        tp = []
        pp = []
        for j in range(int(np.ceil(tt.shape[0]/float(sT)))):
            lo = j*sT
            hi = min((j+1)*sT,tt.shape[0])
            idx = choice(np.arange(lo,hi))
            tp.append(tt[idx])
            pp.append(ppp[idx])
        tp = np.array(tp)
        pp = np.array(pp)

    if 'P13-MC1-1' in df:
        factor = 120.0
    elif 'P13-MC1-3' in df:
        factor = 200.0
    elif 'P13-MC1-5' in df:
        factor = 142.5
    elif 'P13-MC1-7' in df:
        factor = 81.0
    else:  # 9
        factor = 82.0
        
    logt = np.log(tt)
    nt = tp.shape[0]
    
    s1fcn = spline(logt,ppp,s=np.linalg.norm(ppp)/factor)
    
    sp1 = np.empty((nt,2))
    for j in range(nt):
        sp1[j,:] = s1fcn.derivatives(np.log(tp[j]))[0:2]
    
    fig = plt.figure(1)
    ax = fig.add_subplot(211)
    ax.plot(tp,pp,'k*')
    ax.plot(tt,ppp,'r.')
    ax.plot(tp,sp1[:,0],'k-')
    ax.set_xlabel('$t$ (sec)')
    ax.set_ylabel('$h$ (m)')
    ax2 = ax.twinx()
    ax2.plot(tp,sp1[:,1],'g--')
    ax.set_xlim([tt[0],tt[-1]])
    ax2.set_ylabel('$\\partial h/\\partial(\\ln(t))$')
    
    ax = fig.add_subplot(212)
    ax.semilogx(tp,pp,'k*')
    ax.semilogx(tt,ppp,'r.')
    ax.semilogx(tp,sp1[:,0],'k-')
    ax.set_xlabel('$t$ (sec)')
    ax.set_ylabel('$h$ (m)')
    ax2.set_title(df.replace('.dat','') + ' factor=%.3g' %factor)
    ax2 = ax.twinx()
    ax2.semilogx(tp,sp1[:,1],'g--')
    ax2.set_xlim([tt[0],tt[-1]])
    ax2.set_ylabel('$\\partial h/\\partial(\\ln(t))$')
    
    # tab-delimited file with thinned times and derivative
    output = np.concatenate((tp[:,None],sp1[:,1][:,None]),axis=1)
    np.savetxt(df.replace('.dat','.txt'),output,fmt='%.6g',delimiter='\t')
    
    # tab-delimited file with thinned drawdown and derivative, and file of just thinned time
    output2 = np.concatenate((pp[:,None],sp1[:,1][:,None]),axis=1)
    np.savetxt(df.replace('.dat','-thinned-head-and-derivative.csv'),output2,fmt='%.6g',delimiter=',')
    np.savetxt(df.replace('.dat','-thinned-time.csv'),tp,fmt='%.6g',delimiter=',')

    plt.tight_layout()
    plt.savefig(df.replace('.dat','.png'))
    plt.close(1)

    # create single PEST instruction file for reading model outputs
    fh = open(df.replace('.dat','.ins'),'w')
    fh.write('@pif\n@----------------------------@\n')
    for j in range(nt):
        fh.write('l1 [obs%3.3i]28:50 [dbs%3.3i]53:75\n' % (j+1,j+1))
    fh.close()

    # create single PEST instruction file for reading model outputs
    fh = open(df.replace('.dat','_noderiv.ins'),'w')
    fh.write('@pif\n@----------------------------@\n')
    for j in range(nt):
        fh.write('l1 [obs%3.3i]28:50 \n' % (j+1))
    fh.close()

    # create data section of PEST control file
    fh = open(df.replace('.dat','_data_section.pst'),'w')
    fh.write('* observation data\n')
    for j in range(nt):
        fh.write('obs%3.3i  %.5g  %.2f obsgroup\n' % (j+1,pp[j],headweight))
    for j in range(nt):
        fh.write('dbs%3.3i  %.5g  %.2f dbsgroup\n' % (j+1,sp1[j,1],derivweight))
    fh.close()

    # create "time" file for forward model
    fh = open(df.replace('.dat','_time.dat'),'w')
    for j in range(nt):
        fh.write('%.5g\n' % (tp[j]))
    fh.close()

    
