import subprocess
import itertools
import time

infile = """F  F  F                  	     :: suppress output to screen?, output dimensionless t?, log output?
5.81  F  7.67227548012707 5.960502431498750475               :: saturated thickness (B), compute L/Le?, L, Le
0.0065 T 7.82 5.87   .0901734335050755  4.2930254741083438    :: robs, computeOmegao, omegao, gammao, LLeo, LLo
3.9  5.11              :: r, z observation locations
5.125  4.775     	     :: l,d; depth to bottom, top of test interval
0.0315  0.0065     	     :: rw,rc; wellbore, tubing radius
.5328440816557248327 .001633247339317149   .5328440816557248327   0.02 3.87 0.01       .001633247339317149  :: K1,2,3;del1,2,3;Kz
1.776610324138889E-4 .400000000000000022         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
%i   %.1g   %.1g           :: deHoog invlap;  M,alpha,tol
%i   %i                         :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
%i   %i  %i  %i                :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
-2  1  30                   :: logspace times; lo, hi, # times to compute
T  171  times.dat             :: compute times?, # times to read, filename for times (1 time/line)
sensitivity-%i-%.1g-%i-%i-%i-%i.out"""

dhMv = [25, 35, 45]
dhav = [1.0E-8, 1.0E-10, 1.0E-12]
tskv = [4, 7, 10]
glsv = [1, 2, 3]
glzv = [7, 10, 13]
glav = [50, 100, 150]

for dhM in dhMv:
    for dha in dhav:
        for tsk in tskv:
            for gls in glsv:
                for glz in glzv:
                    for gla in glav:
                        fh = open('input-gen.dat','w')
                        fh.write(infile % (dhM,dha,dha/10.0, tsk,tsk-2, gls,gls,glz,gla,
                                           dhM,dha,tsk,gls,glz,gla))
                        fh.close()
                        p = subprocess.Popen('./genslug input-gen.dat', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                        t0 = time.time()
                        stdout,stderr = p.communicate()
                        print dhM,dha,tsk,gls,glz,gla,time.time()-t0

                        fh = open('sensitivity-%i-%.1g-%i-%i-%i-%i.stdout' % (dhM,dha,tsk,gls,glz,gla),'w')
                        fh.write(stdout)
                        fh.close()

                        fh = open('sensitivity-%i-%.1g-%i-%i-%i-%i.stderr' % (dhM,dha,tsk,gls,glz,gla),'w')
                        fh.write(stderr)
                        fh.close()

