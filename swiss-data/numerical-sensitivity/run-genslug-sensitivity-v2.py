import subprocess
import itertools
import time

infile = """T  F  F                  	     :: suppress output to screen?, output dimensionless t?, log output?
5.81  F  7.67227548012707 5.960502431498750475               :: saturated thickness (B), compute L/Le?, L, Le
0.0065 T 7.82 5.87   .0901734335050755  4.2930254741083438    :: robs, computeOmegao, omegao, gammao, LLeo, LLo
3.9  5.11              :: r, z observation locations
5.125  4.775     	     :: l,d; depth to bottom, top of test interval
0.0315  0.0065     	     :: rw,rc; wellbore, tubing radius
.5328440816557248327 .001633247339317149   .5328440816557248327   0.02 3.87 0.01       .001633247339317149  :: K1,2,3;del1,2,3;Kz
1.776610324138889E-4 .400000000000000022         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
25   1.0E-12   1.0E-13           :: deHoog invlap;  M,alpha,tol
%i   %i                         :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
%i   %i  8  50                :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
-2  1  30                   :: logspace times; lo, hi, # times to compute
F  30  times.dat             :: compute times?, # times to read, filename for times (1 time/line)
sensitivity-v2-%i-%i-%i.out"""

tskv = [8, 10, 12]
gls1v = [1, 2, 3, 4, 5, 6, 7, 8]
gls2v = [1, 2, 3, 4, 5, 6, 7, 8]

for tsk in tskv:
    for gls1 in gls1v:
        for gls2 in gls2v:
            if gls1 == gls2:
                fh = open('input-gen.dat','w')
                fh.write(infile % (tsk,tsk-2, gls1,gls2, tsk,gls1,gls2))
                fh.close()
                p = subprocess.Popen('./genslug input-gen.dat', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                t0 = time.time()
                stdout,stderr = p.communicate()
                print tsk,gls1,gls2,time.time()-t0
                


