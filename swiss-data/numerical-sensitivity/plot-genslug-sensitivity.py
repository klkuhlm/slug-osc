import numpy as np
import matplotlib.pyplot as plt


dhMv = [25, 35, 45]
dhav = [1.0E-8, 1.0E-10, 1.0E-12]
tskv = [4, 7, 10]
glsv = [1, 2, 3]
glzv = [7, 10, 13]
glav = [50, 100, 150]

colors = ['red','green','blue']

d = np.zeros((3,3,3,3,3,3,30,3),dtype=np.float32)

for i,dhM in enumerate(dhMv):
    for j,dha in enumerate(dhav):
        for k,tsk in enumerate(tskv):
            for l,gls in enumerate(glsv):
                for m,glz in enumerate(glzv):
                    for n,gla in enumerate(glav):

                        d[i,j,k,l,m,n,:,0:3] = np.loadtxt('sensitivity-%i-%.1g-%i-%i-%i-%i.out' % (dhM,dha,tsk,gls,glz,gla))

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with deHoog M
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i,dhM in enumerate(dhMv):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                for m in range(3):
                    for n in range(3):
    
                        axr.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,1],'k-',color=colors[i])
                        axd.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,2],'k-',color=colors[i])

plt.savefig('all-sensitivity-dHM-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with deHoog alpha/tol
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i in range(3):
    for j,dha in enumerate(dhav):
        for k in range(3):
            for l in range(3):
                for m in range(3):
                    for n in range(3):
    
                        axr.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,1],'k-',color=colors[j])
                        axd.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,2],'k-',color=colors[j])

plt.savefig('all-sensitivity-dHalpha-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with tanh-sinh order (k)
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i in range(3):
    for j in range(3):
        for k,tsk in enumerate(tskv):
            for l in range(3):
                for m in range(3):
                    for n in range(3):
    
                        axr.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,1],'k-',color=colors[k])
                        axd.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,2],'k-',color=colors[k])

plt.savefig('all-sensitivity-tsk-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with Gauss-Lobatto split
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i in range(3):
    for j in range(3):
        for k in range(3):
            for l,gls in enumerate(glsv):
                for m in range(3):
                    for n in range(3):
    
                        axr.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,1],'k-',color=colors[l])
                        axd.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,2],'k-',color=colors[l])

plt.savefig('all-sensitivity-gls-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with Gauss-Lobatto num zeros
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                for m,glz in enumerate(glzv):
                    for n in range(3):
    
                        axr.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,1],'k-',color=colors[m])
                        axd.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,2],'k-',color=colors[m])

plt.savefig('all-sensitivity-glz-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with Gauss-Lobatto abccisa per interval
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                for m in range(3):
                    for n,gla in enumerate(glav):
    
                        axr.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,1],'k-',color=colors[n])
                        axd.plot(d[i,j,k,l,m,n,:,0],d[i,j,k,l,m,n,:,2],'k-',color=colors[n])

plt.savefig('all-sensitivity-gla-results.png')
plt.close(1)

