import subprocess
import itertools
import time

infile = """F  F  F  %s  %.1g                  	     :: suppress output to screen?, output dimensionless t?, log output?, accelerate Hankel sum?, radius of finite domain
5.81  F  7.6722 5.9605               :: saturated thickness (B), compute L/Le?, L, Le
0.0065 T 7.82 5.87   0.090173  4.2930    :: robs, computeOmegao, omegao, gammao, LLeo, LLo
3.9  5.11              :: r, z observation locations
5.125  4.775     	     :: l,d; depth to bottom, top of test interval
0.0315  0.0065     	     :: rw,rc; wellbore, tubing radius
0.53284 0.0016332  0.53284   0.02 3.87 0.01  0.0016332  :: K1,2,3;del1,2,3;Kz
1.77661E-4  0.40         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
25   1.0E-12 1.0E-13   %i           :: deHoog invlap;  M,alpha,tol ;; # terms in Hankel finite sum
-2  1  30                   :: logspace times; lo, hi, # times to compute
T  171  times.dat             :: compute times?, # times to read, filename for times (1 time/line)
sensitivity-%s-%.3g-%i-%s.out"""
 
excv = ['./genslug-finite']
accv = ['F','T']
domv = [8.0, 16.0, 32.0, 64.0]
hnkv = [100, 500, 1000, 2000]

for exc in excv:
    for acc in accv:
        for dom in domv:
            for hnk in hnkv:
                fh = open('input-finite.dat','w')
                if exc == './genslug-finite':
                    prec = 'EP'
                else:
                    prec = 'QP'
                fh.write(infile % (acc,dom,hnk, acc,dom,hnk,prec))
                fh.close()
                p = subprocess.Popen('%s input-finite.dat' % exc, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                t0 = time.time()
                stdout,stderr = p.communicate()
                print acc,dom,hnk,prec,time.time()-t0

                fh = open('sensitivity-%s-%.1g-%i-%s.stdout' % (acc,dom,hnk,prec),'w')
                fh.write(stdout)
                fh.close()

                fh = open('sensitivity-%s-%.1g-%i-%s.stderr' % (acc,dom,hnk,prec),'w')
                fh.write(stderr)
                fh.close()

