import numpy as np
import matplotlib.pyplot as plt

excv = ['./genslug-finite','./genslug-finite-QP']
accv = ['F']
domv = [8.0, 32.0, 128.0, 512.0]
hnkv = [500, 1000, 2000]

colors = ['red','green','blue','black']

d = np.zeros((2,4,3,30,3),dtype=np.float32)

for i,exc in enumerate(excv):
    for k,dom in enumerate(domv):
        for l,hnk in enumerate(hnkv):
            if exc == './genslug-finite':
                prec = 'EP'
            else:
                prec = 'QP'
            d[i,k,l,:,0:3] = np.loadtxt('sensitivity-F-%.3g-%i-%s.out' % (dom,hnk,prec))

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with excutable (QP vs EP)
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i,exc in enumerate(excv):
    for k,dom in enumerate(domv):
        for l,hnk in enumerate(hnkv):   
            axr.plot(d[i,k,l,:,0],d[i,k,l,:,1],'k-',color=colors[i])
            axd.plot(d[i,k,l,:,0],d[i,k,l,:,2],'k-',color=colors[i])

plt.savefig('all-sensitivity-prec-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with finite domain size
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i,exc in enumerate(excv):
    for k,dom in enumerate(domv):
        for l,hnk in enumerate(hnkv):   
            axr.plot(d[i,k,l,:,0],d[i,k,l,:,1],'k-',color=colors[k])
            axd.plot(d[i,k,l,:,0],d[i,k,l,:,2],'k-',color=colors[k])

plt.savefig('all-sensitivity-domain-size-results.png')
plt.close(1)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# plot variation with number Hankel terms
fig = plt.figure(1,figsize=(17,11))
axr = fig.add_subplot(1,2,1)
axd = fig.add_subplot(1,2,2)

for i,exc in enumerate(excv):
    for k,dom in enumerate(domv):
        for l,hnk in enumerate(hnkv):   
            axr.plot(d[i,k,l,:,0],d[i,k,l,:,1],'k-',color=colors[l])
            axd.plot(d[i,k,l,:,0],d[i,k,l,:,2],'k-',color=colors[l])

plt.savefig('all-sensitivity-Hankel-terms-results.png')
plt.close(1)
                
