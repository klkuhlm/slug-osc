\documentclass[draft,wrr]{agutex}
\usepackage{lineno}
\usepackage[dvips]{graphicx}
\setkeys{Gin}{draft=false}

\usepackage{setspace}
\usepackage{amsmath,amssymb}
%\usepackage{bm}
\usepackage{multirow} 

% Needed to get lineno to work nicely with amsmath
\newcommand*\patchAmsMathEnvironmentForLineno[1]{
 \expandafter\let\csname old#1\expandafter\endcsname\csname #1\endcsname
 \expandafter\let\csname oldend#1\expandafter\endcsname\csname end#1\endcsname
 \renewenvironment{#1}
 {\linenomath\csname old#1\endcsname}
 {\csname oldend#1\endcsname\endlinenomath}}
\newcommand*\patchBothAmsMathEnvironmentsForLineno[1]{
 \patchAmsMathEnvironmentForLineno{#1}
 \patchAmsMathEnvironmentForLineno{#1*}}
\AtBeginDocument{
\patchBothAmsMathEnvironmentsForLineno{equation}
\patchBothAmsMathEnvironmentsForLineno{align}}

\everymath{\displaystyle}

\authorrunninghead{BRAUCHLER ET AL.}
\titlerunninghead{Crosshole unconfined slug tests}

% Authors address in author-alphabetical order
\authoraddr{Ralf Brauchler ETH, Zurich,\\
Kristopher L. Kuhlman, Sandia National Laboratories, Albuquerque, New Mexico,\\
Bwalya Malama, California Polytechnic State University, San Luis Obispo, California}

\begin{document}
\begin{linenumbers}
\title{Modeling cross-hole slug tests in an unconfined aquifer}

\author{Ralf Brauchler}
\affil{ETH}
\author{Bwalya Malama}
\affil{California Polytechnic State University}
\author{Kristopher L. Kuhlman}
\affil{Sandia National Laboratories}

\begin{abstract}
The unconfined aquifer model was fitted to slug test observation well data. Table \ref{tab:params} gives the estimated parameter values. It should be noted that for test 2, the plotted model response is multiplied by $-1$, i.e., model and data were $\pi$ out of phase. Also, $L$ and $L_e$ are fixed as per their respective definitions. Improved fits may be obtainable if one of these were estimated.
\end{abstract}

%\begin{keyword}
%Slug tests, unconfined aquifer, skin, hydraulic conductivity, specific storage, specific yield
%\end{keyword}
%\end{frontmatter}

\begin{article}

\section{Introduction}
Slug tests are a common hydrogeology tool for hydraulic characterization of aquifers because compared to constant-rate pumping tests, they are quick to conduct, obviate the need for waste water disposal, require less equipment, and are not as labor intensive. Fundamentally, they involve instantaneous (step or pulse) elevation of fluid pressure in a well followed by continuous monitoring of the pressure decline as the pressure perturbation dissipates by fluid flow into the aquifer formation. This is typically achieved by either dropping a slug mass \citep{cooper1967} into or pneumatically pressurizing the water column in a well \citep{butler1998, malama2011}, a configuration referred to as a single well test. Several models are available in the hydrogeology literature for analyzing confined \citep{cooper1967, bredehoeft1980, zurbuchen2002, butler2004} and unconfined \citep{bouwer1976, springer1991, hyder1994, spane1996a, zlotnik1998, malama2011} aquifer slug test data. Slug tests have the advantage of only involving limited contact with and minimal disposal of effluent formation water. As such, they have found wide application for characterizing heterogeneous formations at contaminated sites \citep{shapiro1998}. However, the small volumes of water involved impose a physical limit on the volume of the formation stressed during the tests \citep{shapiro1998, beckie2002} and that the resulting pressure perturbations do not propagate over sufficiently large distances to be measurable in an observation well. As a result, hydraulic parameters estimated from single well slug-test data are only applicable to the vicinity of the source well \citep{butler2005, beckie2002}.

Cross-hole (or multi-well) tests are less common but have been applied more extensively to interrogate relatively large formation volumes. For example \citet{vesselinov2001b} and \citet{illman2001} used pneumatic cross-hole \textit{injection} tests to hydraulically characterized a fractured unsaturated rock formation with dimensions of $30\times 30\times 30\;\mathrm{m}^3$. \citet{barker1983} presented evidence of measurable pressure responses in observation wells at radial distances of several meters from the source well. \citet{audouin2008} reported cross-hole slug tests conducted in fractured rock, where they collected data in observations wells at radial distances ranging from 30 to about 120 m from the source well, and observed maximum peak amplitudes ranging from 5 to 20 cm. This demonstrated empirically that slug test pressure perturbations can propagate over relatively large distances beyond the immediate vicinity of the source well, albeit for fractured rocks, which are characterized by high hydraulic diffusivities. \citet{brauchler2010} attempted to intensively apply cross-hole slug tests to obtained a detailed image of confined aquifer heterogeneity. They used the model of \citet{butler2004} to estimate aquifer hydraulic conductivity, specific storage and anisotropy. Cross-hole slug tests in unconfined aquifers, neglecting wellbore inertial effects, have been reported by \citet{spane1996a}, \citet{spane1996b}, and \citet{belitz1999} for source-to-observation well distances not exceeding 15 m.

The need still exists to characterize heterogeneous highly permeable unconfined aquifers using cross-hole slug tests where source and observation well inertial effects are not negligible. Recently, \citet{malama2011} developed a slug test model for unconfined aquifers using the linearized kinematic condition of \citet{neuman1972} at the water table, and accounting for inertial effects of the source well. They analyzed data from single well tests performed in a shallow unconfined aquifer. This work extends the application of the model of \citet{malama2011} to data collected in observation wells that also exhibit inertial effects. The data are used to estimate hydraulic conductivity, anisotropy, specific storage, and specific yield.

\section{Slug test model}
\citet{malama2011} developed a model for formation and source well response to slug tests performed in unconfined aquifers. The model is based on the linearized kinematic condition at the water table and accounts for source well inertial effects. A schematic of the conceptual basis for the model is shown in Figure \ref{fig:slugschematic}. The model is presented as follows (see \citet{malama2011} for details of derivation)
\begin{equation}
\label{eqn:}
\hat{\overline{s}}_D = \left\{
  \begin{split}
   \hat{\overline{s}}_D^{(1)} & \quad \mbox{$\forall z_D\in[0,d_D]$}\\
   \hat{\overline{s}}_D^{(2)} & \quad \mbox{$\forall z_D\in[d_D,l_D]$}\\
   \hat{\overline{s}}_D^{(3)} & \quad \mbox{$\forall z_D\in[l_D,1]$},
  \end{split} \right.
\end{equation}
where $\hat{\overline{s}}_D$ is the Laplace and Hankel transformed dimensionless formation response $s_D$, $d_D=d/B$ and $l_D=l/B$ are the normalized depths to the top and bottom of the screened test interval, respectively. The functions $\hat{\overline{s}}_D^{(i)}$ for $i=1,2,3$ are defined by
\begin{align}
\hat{\overline{s}}_D^{(1)} & = \frac{\Delta_1(z_D)}{\Delta_1(d_D)} \hat{\overline{s}}_D^{(2)} |_{z_D=d_D},\\
\hat{\overline{s}}_D^{(2)} & = \hat{\overline{u}}_D + \hat{\overline{v}}_D,\\
\hat{\overline{s}}_D^{(3)} & = \frac{\cosh(\eta z_D^\ast)}{\cosh(\eta l_D^\ast)} \hat{\overline{s}}_D^{(2)}|_{z_D=l_D},
\end{align}
where $z_D=z/B$, $z_D^\ast=1-z_D$, $l_D^\ast = 1 - l_D$, $\eta = \sqrt{(p+a^2)/\kappa}$, $p$ and $a$ are the Laplace and Hankel transform parameters, respectively, 
\begin{equation}
\hat{\overline{u}}_D = \frac{C_D(1-p\overline{H}_D)}{\kappa\eta^2\xi_w\mathrm{K}_1(\xi_w)},
\end{equation}
\begin{equation}
\label{eqn:vD}
 \hat{\overline{v}}_D = -\frac{\hat{\overline{u}}_D}{\Delta_0(1)}[\Delta_0(d_D) \cosh(\eta z_D^\ast) + \sinh(\eta \l_D^\ast)\Delta_1 (z_D)],
\end{equation}
\begin{equation}
\Delta_0 (z_D) = \sinh(\eta z_D) + \varepsilon \cosh(\eta z_D),
\end{equation}
\begin{equation}
\Delta_1 (z_D) = \cosh(\eta z_D) + \varepsilon \sinh(\eta z_D),
\end{equation}
$C_D=r_{D,c}^2/(b_s S_s)$ is the dimensionless source wellbore storage coefficient and , $b_s$ is the length of the source well screened interval, $\kappa = K_z/K_r$ is the formation anisotropy ratio, $\xi_w=r_{D,w}\sqrt{p}$, $\varepsilon=p/(\eta\alpha_D)$, and $\mathrm{K}_1()$ is the first-order modified Bessel function of the second kind. The function $\overline{H}_D$ is the Laplace transform of $H_D = H(t)/H_0$, the normalized response in the source well, and is given by
\begin{align}
\label{eqn:solution2}
\overline{H}_D(p) & = \frac{\overline{f}_1(p)}{\omega_{D,s}^2 + p\overline{f}_1(p)},
\end{align}
where $\omega_{D,s} = \omega_s T_c$, $\omega_s = \sqrt{g/L_e}$ is the source well frequency, $T_c = B^2/\alpha_r$ is the characteristic time, and
\begin{align}
\overline{f}_1(p) & = p + \gamma_{D,s} + \frac{1}{2}\omega_{D,s}^2\overline{\Omega}.
\end{align}
The function $\overline{\Omega}$ is defined by
\begin{equation}
\label{eqn:Lap_Omega}
\overline{\Omega}(r_{D,w},p) = \mathcal{H}_0^{-1}\{\hat{\overline{\Omega}} (a,p)\}|_{r_{D,w}},
\end{equation}
where $\mathcal{H}_0^{-1}\{\}$ is the inverse zeroth-order Hankel transform operator and
\begin{equation}
\label{eqn:HankLap_Omega}
\hat{\overline{\Omega}} (a,p) = \frac{C_D[1 - \langle \hat{\overline{w}}_D (a,p) \rangle]}{\kappa \eta^2 \xi_w \mathrm{K}_1(\xi_w)}.
\end{equation}
\citet{malama2011} showed that
\begin{equation}
\label{eqn:wD}
\langle \hat{\overline{w}}_D \rangle = \frac{1}{b_{D,s}\eta \Delta_0(1)} \{\Delta_0(d_D) \sinh(\eta d_D^\ast) + [\Delta_0(l_D) - 2\Delta_0(d_D)] \sinh(\eta l_D^\ast)\},
\end{equation}
where $d_D^\ast = 1-d_D$, $b_{D,s}=b_s/B$, $z_D = z/B$, $r_{D,w} = r_w/B$ is the dimensionlesswellbore radius, $\gamma_{D,s} = \gamma_s T_c$, $\gamma_s$ is the source well damping coefficient, $\nu$ is the kinematic viscosity of water. $\overline{H}_D(p)$ is the Laplace transform of $H_D(t_D)$. According to \citet{butler2004} the source well damping coefficient may be estimated as $\gamma_s = 8\nu L /(L_e r_c^2)$.

\subsection{Observation \& pumping well skin - approximate approach}
\textit{I think we need to draw a simple figure to more clearly explain this approach.}

It is assumed here that the slug response at the observation well is due to fluid mass flow through the subdomains associated with the pumping and observation wells and the formation. Pumping and observation well skin and formation hydraulic conductivities, $K_i$, $i=1,2,3$, are arranged in series for radial flow, and in parallel for vertical flow. Hence, the effective radial and vertical hydraulic conductivity, $\langle K_r \rangle$ and $\langle K_z \rangle$, of the formation between the pumping and observation wells may be obtained from the well-known relations
\begin{equation}
\label{eqn:eff-Kr}
\frac{\delta_T}{\langle K_r \rangle} = \sum_{n=1}^3 \frac{\delta_i}{K_i},
\end{equation}
and
\begin{equation}
\label{eqn:eff-Kz}
\langle K_z \rangle = \frac{1}{\delta_T} \sum_{n=1}^3 \delta_i K_i,
\end{equation}
where $\delta_T=\sum_{n=1}^3 \delta_i$ and $\delta_i$ is the thickness of the $i^\mathrm{th}$ zone along $r$. For the purposes of the present study it is assumed that $K_r \approx\langle K_r \rangle$ and $K_z \approx\langle K_z \rangle$, i.e., Equations (\ref{eqn:eff-Kr}) and (\ref{eqn:eff-Kz}) are used for radial and vertical hydraulic conductivities in the computation of the solution developed above. This follows the precedence set by \citet{shapiro1998} for using the equivalent hydraulic conductivity approach to account of simple heterogeneity.

\subsection{Observation well storage \& inertial effects}
We incorporate observation well storage and inertial effects by the following momentum balance equation,
\begin{equation}
\frac{d^2 s_{\mathrm{obs}}}{d t^2} + \gamma_o \frac{d s_{\mathrm{obs}}}{d t} + \omega_o^2 s_{\mathrm{obs}} = \omega_o^2 \langle s \rangle
\end{equation}
where $\gamma_o$ and $\omega_o$ are observation well damping coefficient and characteristic frequency, respectively. From \citet{butler2004}, $\gamma_o \sim f(r_{c,\mathrm{obs}},r_{w,\mathrm{obs}}, g/L_{e,\mathrm{obs}})$, and $\omega_o \sim f(K_s,g/L_{e,\mathrm{obs}})$. In this work an attempt will be made to estimate $\gamma_o$ and $\omega_o$ directly from observation well data. The dimensionless form of the above equation is
\begin{equation}
\frac{d^2 s_{D,\mathrm{obs}}}{d t_D^2} + \gamma_{D,o} \frac{d s_{D,\mathrm{obs}}}{d t_D} + \omega_{D,o}^2 s_{D,\mathrm{obs}} = \omega_{D,o}^2 \langle s_D \rangle
\end{equation}
where $\gamma_{D,o}$ is dimensionless observation well damping coefficient, $\omega_{D,o}$ is dimensionless observation well characteristic frequency, $\langle s_D \rangle$ is dimensionless formation response averaged over the observation well screen length, and $s_{D,\mathrm{obs}}$ is the observed (dimensionless) response in the well. Applying the laplace transform and solving for $s_\mathrm{obs}$ gives
\begin{equation}
\overline{s}_{D,\mathrm{obs}} = \overline{f}_2(p) \langle \overline{s}_D (r_D,p) \rangle,
\end{equation}
where $\overline{f}_2(p) = \omega_{D,o}^2/(p^2 + p \gamma_{D,o} + \omega_{D,o}^2)$.
\begin{equation}
\label{eqn:s_obs}
\langle \hat{\overline{s}}_D \rangle = \left\{
  \begin{split}
   \langle \hat{\overline{s}}_D^{(1)} \rangle & \quad \mbox{$\forall z_D\in[0,d_D]$}\\
   \langle \hat{\overline{s}}_D^{(2)} \rangle & \quad \mbox{$\forall z_D\in[d_D,l_D]$}\\
   \langle \hat{\overline{s}}_D^{(3)} \rangle & \quad \mbox{$\forall z_D\in[l_D,1]$},
  \end{split} \right.
\end{equation}

\begin{align}
\langle \hat{\overline{s}}_D^{(1)} \rangle & = \left[\frac{\Delta_0(l_{D,o}) - \Delta_0(d_{D,o})}{\eta b_{D,o} \Delta_1(d_D)}\right] \hat{\overline{s}}_D^{(2)}|_{z_D=d_D}\\
\langle \hat{\overline{s}}_D^{(2)} \rangle & = \hat{\overline{u}}_D + \langle \hat{\overline{v}}_D \rangle,\\
\langle \hat{\overline{s}}_D^{(3)} \rangle & = \frac{\Delta_2}{\eta b_{D,o} \cosh(\eta l_D^\ast)} \hat{\overline{s}}_D^{(2)}|_{z_D=l_D}.
\end{align}
\begin{align}
 \langle \hat{\overline{v}}_D \rangle & = -\frac{\hat{\overline{u}}_D}{\eta b_{D,o}\Delta_0(1)}\left\{\Delta_0(d_D)\Delta_2 - [\Delta_0(d_{D,o}) - \Delta_0(l_{D,o})] \sinh(\eta \l_D^\ast) \right\}.
\end{align}
\begin{equation}
\Delta_2 = \sinh(\eta d_{D,o}^\ast) - \sinh(\eta l_{D,o}^\ast)
\end{equation}

Note that, upon inverting the Laplace transform, one obtains
\begin{equation}
s_{D,\mathrm{obs}} = \int_0^{t_D} f_2(t_D-\tau) \langle s_D (r_D,\tau) \rangle d \tau
\end{equation}
with $f_2(t) = \mathcal{L}^{-1} \{\bar{f}_2(p)\}$.

\section{Model application to cross-hole slug test data}
The model described above is applied to observations collected in a series of multi-level cross-well pneumatic slug tests performed in June 2013 at the Widen site in Switzerland. The wells used in the experiments are completed in an unconfined sand and gravel aquifer with a saturated thickness of 5.81 m. The source-well consists of a HDPE (high density polyethlyne) casing that is screened across the entire saturated thickness of the aquifer.

\subsection{Experimental procedure}
The cross-well pneumatic slug tests were initiated by applying gas pressure to the water column in a chosen interval, then releasing the gas pressure through an out-flow valve to provide the instantaneous initial slug. A double-packer system straddling a screened (test) interval of $b_s = 0.35$ m was used in the source well (well P13) or radius $r_w = 3.15\times 10^{-2}$ m. The pneumatic slug was applied in a tubing of radius $r_c = 1.55\times 10^{-2}$ m. The dissipation of the slug was monitored with a pressure transducer in the source well positioned close to the water table (top of water column) above the test interval. The source-well transducer was placed close to the water table in order to minimize the effects associated with water column acceleration.

The response of the formation reported here were monitored in three observations wells labeled MC1, MC2, and MC4 and located at radial distances from the source well of 3.9, 2.9, and 2.8 m. The responses at different vertical positions in each observation well were monitored with pressure transducers in a seven channel CMT multilevel system with screen intervals of $b_o = 8\times 10^{-2}$ m. Each channel in the CMT multilevel system has an equivalent radius of $r_{c,o}=6.5\times 10^{-3}$ m. However, installation of a pressure transducer in these channels reduces their effective radii significantly. The CMT system allows for simultaneous monitoring of the response at seven vertical positions for each slug test. In this work, only data from the observation intervals at approximately the same vertical position as the source-well test interval are considered. Pressure responses were recorded at frequency of 50 Hz (every 0.02 seconds) for a period of about 20 seconds from slug initiation. An example experimental system setup is shown in Figure \ref{fig:setup}.

\subsection{Experimental Results and Observations}
The typical slug test responses observed during tests at the Widen site are shown in Figure \ref{fig:typical-obs}. The plots in Figure \ref{fig:typical-obs}(a) are the source well responses, and those in (b) are the corresponding responses in an observation well at about 3 m from the source well. The results clearly show that damped oscillations generated in the source well are measurable in an observation well a few meters away. Comparing the results in (a) and (b) also shows that the maximum amplitude of the signal decays about two orders of magnitude from the source to the observation well. This has the effect of decreasing the signal-to-noise ratio.

The observation well responses also show a general decrease in damping with increasing depth from the water table even when the initial displacements from the equilibrium position are comparable. This is evident in the data from all three profiles shown in Figure \ref{fig:typical-obs}, where data closer to the water table appear to be more damped than those at greater depths. It should be noted however that measurable displacement are still obtainable near the water table. The configuration of the equipment made it physically impossible to record the response at the water table. Placing a pressure transducer at the water table would establish whether using a constant head boundary condition there (see \citet{hyder1994}) is appropriate.

\subsection{Parameter estimation procedure and results}
The model discussed above was used to estimate parameters from data collected in observation wells during the tests at the Widen site. The aquifer is assumed to be isotropic, $K_r = K_z = K$, and the skin conductivities of the source and observation wells are assumed to be equal, i.e., $K_1 = K_3 = K_\mathrm{skin}$. The estimated parameters are skin hydraulic conductivity $K_\mathrm{skin}$, formation hydraulic conductivity $K$, specific storage $S_s$, and specific yield, $S_y$, and the length parameters $L$ and $L_e$ that characterize the source well damping coefficient and frequency. It is typical to compute $L$ and $L_e$ using the formulas \citep{butler2002, kipp1985, zurbuchen2002}
\begin{equation}
\label{eqn:L}
L = d + \frac{b}{2} \left(\frac{r_c}{r_w}\right)^4,
\end{equation}
and
\begin{equation}
\label{eqn:Le}
L_e = L + \frac{b}{2} \left(\frac{r_c}{r_w}\right)^2.
\end{equation}
The values of $L$ and $L_e$ computed with these formulas (indicated as $L^\ast$ and $L_e^\ast$ in \ref{tab:params}) are compared with those estimated directly from the data. The parameters $L_{e,\mathrm{obs}}$ and $L_\mathrm{obs}$, which determine the frequency and damping coefficient, respectively, of the observation well were also estimated. The optimization code PEST \citep{doherty2010} was used to estimate the parameters.

Estimated parameters are summarized in Table \ref{tab:params}. The corresponding model fits to observation well data are shown in Figure \ref{fig:modelfits}. The large value of $K_s = 2.25$ m/s estimated from test 1 and used for the other tests is reasonable for a gravel filter pack used in well construction. Estimates of the parameter $L_e$ are comparable to those predicted by equation (\ref{eqn:Le}). However, estimates of $L$ are consistently larger than the values predicted by equation (\ref{eqn:L}).

\subsection{Predictive analysis using DREAM}

\section{Discussion \& Conclusions}

\section*{Notation}
\begin{tabular}{ll}
$K_{r}$ & Radial hydraulic conductivity of formation, $\mathrm{[LT^{-1}]}$ \\ 
$K_{z}$ & Vertical hydraulic conductivity of formation, $\mathrm{[LT^{-1}]}$ \\ 
$K_\mathrm{skin}$ & Skin hydraulic conductivity, $\mathrm{[LT^{-1}]}$ \\ 
$S_{s}$ & Specific storage of formation, $\mathrm{[L^{-1}] }$\\ 
$S_y$ & Specific yield, $[-]$ \\ 
$\alpha_{r,i}$ & Hydraulic diffusivity of $i^\mathrm{th}$ zone, $\mathrm{[L^2T^{-1}]}$ \\ 
$B$ & Aquifer thickness, $\mathrm{[L]}$ \\ 
$z$ & Vertical distance, measured up from water table, $\mathrm{[L]}$  \\
$r$ & Radial distance from center of source well, $\mathrm{[L]}$ \\ 
$t$ & Time since slug initiation, $\mathrm{[T]}$ \\
$r_w$ & Radius of source well at test interval, $\mathrm{[L]}$ \\ 
$r_c$ & Radius of slug test tubing, $\mathrm{[L]}$ \\ 
$C_w$ & Coefficient of wellbore storage,  $\mathrm{[L^2]}$ \\ 
$b_s$ & Length of source well test interval, $\mathrm{[L]}$ \\ 
$d_s$ & Depth to top of source well test interval, $\mathrm{[L]}$ \\ 
$l_s$ & Depth to bottom of source well test interval, $\mathrm{[L]}$ \\ 
$s$ & Head change in formation, $\mathrm{[L]}$ \\
$H$ & Displacement from equilibrium position in source well, $\mathrm{[L]}$ \\
$H_0$ & Initial slug input, $\mathrm{[L]}$ \\ 
$H'_0$ & Initial velocity of slug input, $\mathrm{[L T^{-1}]}$ \\ 
$T_c$ & Characteristic time ($T_c=B^2/\alpha_{r,1}$), $\mathrm{[T]}$ \\
$\nu$ & Kinematic viscosity of water, $\mathrm{[L^2 T^{-1}]}$ \\ 
$g$ & Acceleration due to gravity, $\mathrm{[L T^{-2}]}$ \\ 
$p$ & Laplace transform parameter, $\bar{f}(p) = \int_0^\infty f(t) e^{-pt} \mathrm{d}t$ \\
$a$ & Hankel transform parameter,$f^\ast(a) = \int_0^\infty a f(r) \mathrm{J}_0(a r) \mathrm{d}r$
\end{tabular} 

\bibliographystyle{agu} 
\bibliography{hydrology}

\end{article}

\begin{figure}[h]
\includegraphics[scale=0.5]{slugschematic.eps} 
\caption{\label{fig:slugschematic} Schematic of the cross-hole slug test set-up for an unconfined aquifer.}
\end{figure}

\begin{figure}[h]
\caption{\label{fig:setup} Example experimental system setup for cross-hole slug tests conducted at the Widen site, Switzerland.}
\end{figure}

\begin{figure}[h]
\includegraphics[scale=0.5]{source.eps} \includegraphics[scale=0.5]{profile-p13mc1.eps}\\
\includegraphics[scale=0.5]{profile-p13mc2.eps} \includegraphics[scale=0.5]{profile-p13mc4.eps}
\caption{\label{fig:typical-obs} Typical source (a) and observation (b-d) well responses measured during cross-hole slug tests. Observation well data show decreasing damping with depth relative to the water table for all three profiles.}
\end{figure}

\begin{figure}[h]
  %\centering
  \begin{tabular}{ccc}
  \includegraphics[scale=0.35]{p13mc1-1-obs.eps} & \includegraphics[scale=0.35]{p13mc2-2-obs.eps} & \includegraphics[scale=0.35]{p13mc4-2-obs.eps}\\
  \includegraphics[scale=0.35]{p13mc1-3-obs.eps} & \includegraphics[scale=0.35]{p13mc2-4-obs.eps} & \includegraphics[scale=0.35]{p13mc4-4-obs.eps}\\
  \includegraphics[scale=0.35]{p13mc1-5-obs.eps} & \includegraphics[scale=0.35]{p13mc2-6-obs.eps} & \includegraphics[scale=0.35]{p13mc4-6-obs.eps}\\
  \includegraphics[scale=0.35]{p13mc1-7-obs.eps} & \includegraphics[scale=0.35]{p13mc2-8-obs.eps} & \includegraphics[scale=0.35]{p13mc4-8-obs.eps}
  \end{tabular}
  \caption{\label{fig:modelfits} Model fits to cross-hole slug test data collected along vertical profiles in three observation wells at the Widen Site, Switzerland. The first, second, and third columns correspond to profiles in observation wells 1, 2, and 3 respectively.}
\end{figure}

\begin{table}[ht]
\caption{\label{tab:dimlessparameters}Dimensionless variables and parameters}
\centering
\begin{tabular}{lll}
\hline 
$s_{D,i}$ &=& $s_i/H_0$\\
$H_\mathrm{uc}$ &=& $H(t)/H_0$\\
$r_D$ &=& $r/B$\\
$r_{D,w}$ &=& $r_w/B$\\
$r_{D,c}$ &=& $r_c/B$\\
$r_{D,s}$ &=& $r_s/B$\\
$z_D$ &=& $z/B$\\
$d_D$ &=& $d/B$\\
$t_D$ &=& $\alpha_{r,1} t/B^2$\\
$C_{D}$ &=& $r_{D,c}^2/(bS_s)$\\
$\alpha_{D}$ &=& $\kappa\sigma$\\
$\beta_1$ &=& $8\nu L/(r_c^2 g T_c)$\\
$\beta_2$ &=& $Le/(g T_c^2)$\\
$\beta_D$ &=& $\beta_1/\sqrt{\beta_2}$\\
$\kappa_i$ &=& $K_{z,i}/K_{r,i}$\\
$\sigma$ &=& $BS_s/S_y$\\
$\gamma$ &=& $K_{r,2}/K_{r,1}$\\
$\vartheta$ &=& $2b S_{s,2} (r_w/r_c)^2$\\
$\xi_\mathrm{sk}$ &=& $r_\mathrm{sk}/r_w$\\
$\xi_w$ &=& $r_{D,w}\sqrt{p}$\\
$\eta^2$ &=& $(p+a^2)/\kappa$\\
\hline
\end{tabular} 
\end{table}

\begin{table}[ht]
\caption{\label{tab:params}Estimated parameters.}
\centering
\begin{tabular}{lcccccccccc}
\hline 
     & $\langle K \rangle$ & $K_\mathrm{skin}$ & $S_s$ & $S_y$ & $L$ & $L_e$ & $L^\ast$ & $L_e^\ast$ & $L_\mathrm{obs}$ & $L_{e,\mathrm{obs}}$\\
Test & [m/s] & [m/s] & [1/m]& [-] & [m] & [m] & [m] & [m] & [m] & [m]\\
\hline
P13-MC1-1 & $6.83\times 10^{-4}$ & 0.57 & $6.13\times 10^{-5}$ & 0.40 & 3.91 & 5.91 & 4.78 & 4.83 & $9.77\times 10^{-2}$ & 5.09\\
P13-MC1-3 & $7.92\times 10^{-4}$ & 0.30 & $5.38\times 10^{-5}$ & 0.35 & 3.67 & 4.45 & 3.78 & 3.83 & $4.68\times 10^{-2}$ & 3.43\\
P13-MC1-5 & $4.74\times 10^{-4}$ & 0.11 & $1.16\times 10^{-5}$ & 0.35 & 2.40 & 3.37 & 2.78 & 2.83 & $1.78\times 10^{-2}$ & 3.49\\
P13-MC1-7 & $1.16\times 10^{-3}$ & 0.21 & $2.90\times 10^{-5}$ & 0.35 & 1.40 & 2.25 & 1.78 & 1.83 & $8.77\times 10^{-2}$ & 4.85\\
P13-MC1-9 & $3.10\times 10^{-3}$ & 0.22 & $3.32\times 10^{-5}$ & 0.42 & 1.70 & 0.98 & 0.78 & 0.83 & $1.64\times 10^{-4}$ & 1.07\\
\hline
P13-MC2-2 & $4.27\times 10^{-3}$ & 0.33 & $2.40\times 10^{-4}$ & 0.35 & 4.80 & 5.03 & 4.40 & 4.43 & $1.05\times 10^{-2}$ & 0.08\\
P13-MC2-4 & $1.18\times 10^{-2}$ & 0.31 & $8.75\times 10^{-4}$ & 0.35 & 11.9 & 3.63 & 3.40 & 3.43 & 0.0 & 3.50\\
P13-MC2-6 & $1.17\times 10^{-2}$ & 0.38 & $8.89\times 10^{-4}$ & 0.35 & 4.38 & 2.90 & 2.40 & 2.43 & $1.56\times 10^{-1}$ & 1.81\\
P13-MC2-8 & $2.24\times 10^{-2}$ & 0.32 & $2.52\times 10^{-7}$ & 0.22 & 3.43 & 1.89 & 1.40 & 1.43 & $1.59\times 10^{-3}$ & 1.38\\
\hline
P13-MC4-2 & $3.68\times 10^{-3}$ & 0.19 & $5.84\times 10^{-5}$ & 0.35 & 4.30 & 4.88 & 4.40 & 4.43 & $5.31\times 10^{-2}$ & 4.88\\
P13-MC4-4 & $2.97\times 10^{-2}$ & 0.19 & $5.47\times 10^{-4}$ & 0.35 & 3.51 & 3.60 & 3.39 & 3.43 & $7.94\times 10^{-5}$ & 1.83\\
P13-MC4-6 & $5.06\times 10^{-2}$ & 0.25 & $3.36\times 10^{-4}$ & 0.35 & 2.43 & 2.44 & 2.39 & 2.43 & $1.28\times 10^{-2}$ & 2.20\\
P13-MC4-8 & $6.13\times 10^{-2}$ & 0.25 & $1.44\times 10^{-2}$ & 0.15 & 3.49 & 1.59 & 1.39 & 1.43 & $1.59\times 10^{-4}$ & 1.88\\
\hline
\end{tabular}
\end{table}
\end{linenumbers}

\end{document}