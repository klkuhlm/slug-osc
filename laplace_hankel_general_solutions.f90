!
! Copyright (c) 2011 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

module lap_hank_soln_gen
  implicit none

  private
  public :: unconfined_wellbore_slug_gen

contains

  function unconfined_wellbore_slug_gen(dum) result(fp)
    use shared_data, only :  dD,lD,zD,rD,alphaD,rDw,CD,kappa,lap,PHIuc,layer
    use constants, only : EP,DP
    use utilities, only : ccosh, csinh
    use inverse_Laplace_Transform, only : dehoog_pvalues
    use complex_bessel, only : cbesk

#ifdef INTEL
    use ifport, only : dbesj0 
#endif

    implicit none
    real(EP), intent(in) :: dum  ! scalar integration variable (Hankel parameter)
    complex(EP), dimension(2*lap%M+1) :: fp

    complex(EP), dimension(2*lap%M+1) :: eta, eps, uD, xiw
    complex(DP), dimension(2*lap%M+1) :: K1
    complex(EP), dimension(0:1,2*lap%M+1) :: delta
    real(EP) :: lDs,dDs,zDs
    integer :: i, np,nz,ierr

    np = 2*lap%M+1

    ! pre-compute some intermediate values
    eta(1:np) = sqrt((lap%p + dum**2)/kappa)
    eps(1:np) = lap%p/(eta*alphaD)
    dDs = 1.0_EP - real(dD,EP)
    lDs = 1.0_EP - real(lD,EP)
    zDs = 1.0_EP - real(zD,EP)
    xiw(1:np) = sqrt(lap%p)*rDw
    
    do i=1,np
       call cbesk(z=cmplx(xiw(i),kind=DP),fnu=1.0D0,kode=1,n=1,cy=K1(i),nz=nz,ierr=ierr)
       if (ierr /= 0 .and. ierr /= 3) then
          write(*,*) 'CBESK error',ierr,' nz=',nz, 'z=',xiw(i)
       end if
    end do

    uD(1:np) = CD*(1.0_EP - lap%p*PHIuc)/((lap%p + dum**2)*xiw*K1)
    
    delta(0,1:np) = csinh(eta)    + eps*ccosh(eta)
    delta(1,1:np) = csinh(eta*dD) + eps*ccosh(eta*dD)

    select case(layer)
    case(1)
       ! sD2(zD=dD)
       fp(1:np) = uD*(1.0_EP - (delta(1,:)*ccosh(eta*dDs) + csinh(eta*lDs)*&
            & (ccosh(eta*dD) + eps*csinh(eta*dD)))/delta(0,:))

       fp = fp*((ccosh(eta*zD) + eps*csinh(eta*zD))/(ccosh(eta*dD) + eps*csinh(eta*dD)))
    case(2)
       ! sD2(zD)
       fp(1:np) = uD*(1.0_EP - (delta(1,:)*ccosh(eta*zDs) + csinh(eta*lDs)*&
            & (ccosh(eta*zD) + eps*csinh(eta*zD)))/delta(0,:))
    case(3)
       ! sD2(zD=lD)
       fp(1:np) = uD*(1.0_EP - (delta(1,:)*ccosh(eta*lDs) + csinh(eta*lDs)*&
            & (ccosh(eta*lD) + eps*csinh(eta*lD)))/delta(0,:))

       fp = fp*ccosh(eta*zDs)/ccosh(eta*lDs)
    end select

    fp = dum*dbesj0(real(dum*rD,DP)) * fp(1:np)  
    ! **** hankel transform evaluated at rD? (rDw in PHIuc solution) ***

  end function unconfined_wellbore_slug_gen
end module lap_hank_soln_gen




