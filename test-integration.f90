program test
  use gauss_points, only : Gauss_Quad_Setup

  implicit none
  
  integer, parameter :: DP = selected_real_kind (p=15,r=300)
!  integer, parameter :: EP = selected_real_kind(r=3000)
!  integer, parameter :: DP = selected_real_kind(p=33,r=3000)
  real(DP), parameter :: PI = 4.0_DP*atan(1.0_DP)
!  real(DP), parameter :: PIDP = 4.0_DP*atan(1.0_DP)
  
  integer :: i,j,n, m, k,tsk,nst,tsN
  integer, dimension(4) :: order = [5,10,20,40]
  real(DP), allocatable :: w(:),a(:),fa(:), tmp(:)
  real(DP), allocatable :: tsw(:), tsa(:), tshh(:)
  integer, allocatable :: tskk(:), tsNN(:), ii(:)
  real(DP) :: rslt
  real(DP) :: polerr, Crslt

  nst = 8
  tsk = 12
  tsN = 2**tsk - 1
  allocate(tsw(tsN),tsa(tsN),fa(tsN),ii(tsN),tmp(nst))
  allocate(tskk(nst),tsNN(nst),tshh(nst))
  
  tskk(1:nst) = [(tsk-m, m=nst-1,0,-1)]
  tsNN(1:nst) = 2**tskk - 1
  tshh(1:nst) = 4.0_DP/(2**tskk)
  
  print *, 'test problem 1'
  ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  call tanh_sinh_setup(tskk(nst), 1.0_DP, tsw(1:tsNN(nst)), tsa(1:tsNN(nst)))
  
  do i = 1,tsNN(nst)
     fa(i) = f1(tsa(i))
  end do

  tmp(nst) = sum(tsw*fa)

  do j=nst-1,1,-1
     !  only need to re-compute weights for each subsequent step
     call tanh_sinh_setup(tskk(j), 1.0_DP, tsw(1:tsNN(j)), tsa(1:tsNN(j)))

     ! compute index vector, to slice up solution 
     ! for nst'th turn count regular integers
     ! for (nst-1)'th turn count even integers
     ! for (nst-2)'th turn count every 4th integer, etc...
     ii(1:tsNN(j)) = [( m* 2**(nst-j), m=1,tsNN(j) )]

     ! estimate integral with subset of function evaluations and appropriate weights
     tmp(j) = sum(tsw(1:tsNN(j))*fa(ii(1:tsNN(j))))
        
  end do
     
  call polint(tshh(1:nst), tmp(1:nst), 0.0_DP, Crslt, PolErr)  
  print *, 'result:',Crslt,Crslt-0.25_DP

  do k = 1,4     
     n = order(k)
     if (allocated(w)) then
        deallocate(w,a)
     end if
     if (allocated(fa)) then
        deallocate(fa)
     end if
     allocate(w(n),a(n),fa(n))

     call gauss_quad_setup(n, a, w)

     a = (a + 1.0_DP)/2.0_DP ! scale abcissa
     w = w/sum(w)
     
     do i = 1,n
        fa(i) = f1(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt-0.25_DP
  end do

  
  print *, 'test problem 6'
  ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  do k = 2,12
     n = 2**k-1
     if (allocated(w)) then
        deallocate(w,a,fa)
     end if
     allocate(w(n),a(n),fa(n))

     call tanh_sinh_setup(k, 1.0_DP, w, a)
  
     do i = 1,n
        fa(i) = f6(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt-PI/4.0_DP
  end do

  do k = 1,4     
     n = order(k)
     if (allocated(w)) then
        deallocate(w,a,fa)
     end if
     allocate(w(n),a(n),fa(n))

     call gauss_quad_setup(n, a, w)

     a = (a + 1.0_DP)/2.0_DP ! scale abcissa
     w = w/sum(w)
     
     do i = 1,n
        fa(i) = f6(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt-PI/4.0_DP
  end do

  print *, 'test problem 8'
  ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  do k = 2,12
     n = 2**k-1
     if (allocated(w)) then
        deallocate(w,a,fa)
     end if
     allocate(w(n),a(n),fa(n))

     call tanh_sinh_setup(k, 1.0_DP, w, a)
  
     do i = 1,n
        fa(i) = f8(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt-2.0_DP
  end do

  do k = 1,4     
     n = order(k)
     if (allocated(w)) then
        deallocate(w,a,fa)
     end if
     allocate(w(n),a(n),fa(n))

     call gauss_quad_setup(n, a, w)

     a = (a + 1.0_DP)/2.0_DP ! scale abcissa
     w = w/sum(w)
     
     do i = 1,n
        fa(i) = f8(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt-2.0_DP
  end do

  print *, 'test problem 9'
  ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  do k = 2,12
     n = 2**k-1
     if (allocated(w)) then
        deallocate(w,a,fa)
     end if
     allocate(w(n),a(n),fa(n))

     call tanh_sinh_setup(k, PI/2.0, w, a)
  
     do i = 1,n
        fa(i) = f9(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt+(PI*log(2.0_DP)/2.0_DP)
  end do

  do k = 1,4     
     n = order(k)
     if (allocated(w)) then
        deallocate(w,a,fa)
     end if
     allocate(w(n),a(n),fa(n))

     call gauss_quad_setup(n, a, w)

     a = (a + 1.0_DP)*(PI/2.0)/2.0_DP ! scale abcissa
     w = w/sum(w)
     
     do i = 1,n
        fa(i) = f9(a(i))
     end do
     rslt = sum(w*fa)
     print *, 'result:',k,n,rslt,rslt+(PI*log(2.0_DP)/2.0_DP)
  end do

  ! ************************************************************
  ! ************************************************************
  
contains
  function f1(t)

    real(DP), intent(in) :: t
    real(DP) :: f1

    f1 = t*log(1.0_DP + t)
    
  end function f1

  function f6(t)

    real(DP), intent(in) :: t
    real(DP) :: f6

    f6 = sqrt(1.0_DP - t*t)
    
  end function f6

  function f8(t)

    real(DP), intent(in) :: t
    real(DP) :: f8

    f8 = log(t)**2
    
  end function f8

  function f9(t)

    real(DP), intent(in) :: t
    real(DP) :: f9

    f9 = log(cos(t))
    
  end function f9

  
!! ###################################################
  subroutine tanh_sinh_setup(k,s,w,a)
    implicit none

    real(DP), parameter :: PIOV2 = PI/2.0_DP
    
    integer, intent(in) :: k  ! order
    real(DP), intent(in) :: s  ! upper-limit of range
    real(DP), intent(out), dimension(2**k-1) :: w, a

    integer :: N,r,i
    real(DP) :: h
    real(DP), allocatable :: u(:,:)

    !! compute weights 
    N = 2**k-1
    r = (N-1)/2
    h = 4.0_DP/2**k
    allocate(u(2,N))
       
    u(1,1:N) = PIOV2*cosh(h*[(i, i=-r,r)])
    u(2,1:N) = PIOV2*sinh(h*[(i, i=-r,r)])
    
    a(1:N) = tanh(u(2,1:N))
    w(1:N) = u(1,1:N)/cosh(u(2,:))**2
    w(1:N) = w(1:N)/sum(w(1:N)) ! normalize (remove 2)

    ! map the -1<=x<=1 quadrature interval onto 0<=a<=s
    a = (a + 1.0_DP)*s/2.0_DP

  end subroutine tanh_sinh_setup


  !! ###################################################
  subroutine gauss_lobatto_setup(ord,abcissa,weight)
    use constants, only : PIEP, EP
    implicit none
    integer, intent(in) :: ord
    real(EP), intent(out), dimension(ord-2) :: weight, abcissa
    real(EP), dimension(ord,ord) :: P
    real(EP), dimension(ord) :: x,xold,w
    integer :: i, N, N1, k

    ! leave out the endpoints (abcissa = +1 & -1), since 
    ! they will be the zeros of the Bessel functions
    ! (therefore, e.g., 5th order integration only uses 3 points)
    ! code copied from Matlab routine by Greg von Winckel, at
    ! http://www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=4775

    N = ord-1
    N1 = N+1
    ! first guess
    x = cos(PIEP*[(i, i=0,N)]/N)
    ! initialize Vandermonde matrix
    P = 0.0
    xold = 2.0

    iter: do 
       if (maxval(abs(x-xold)) > spacing(1.0_EP)) then
          xold = x
          P(:,1) = 1.0
          P(:,2) = x
          
          do k = 2,N
             P(:,k+1) = ((2*k-1)*x*P(:,k) - (k-1)*P(:,k-1))/k
          end do
          
          x = xold - (x*P(:,N1)-P(:,N))/(N1*P(:,N1))
       else
          exit iter
       end if
    end do iter
    
    w = 2.0/(N*N1*P(:,N1)**2)

    ! leave off endpoints
    abcissa = x(2:ord-1)
    weight = w(2:ord-1)

  end subroutine gauss_lobatto_setup

    !! ###################################################
  ! polynomial extrapolation modified from numerical recipes f90 (section 3.1)
  SUBROUTINE polint(xa,ya,x,y,dy)
    ! xa and ya are given x and y locations to fit an nth degree polynomial
    ! through.  results is a value y at given location x, with error estimate dy

    use constants, only : DP
    IMPLICIT NONE
    REAL(DP), DIMENSION(:), INTENT(IN) :: xa
    real(DP), dimension(:), intent(in) :: ya
    REAL(DP), INTENT(IN) :: x
    real(DP), INTENT(OUT) :: y,dy
    INTEGER :: m,n,ns
    REAL(DP), DIMENSION(size(xa)) :: c,d,den
    REAL(DP), DIMENSION(size(xa)) :: ho
    integer, dimension(1) :: tmp

    n=size(xa)
    c=ya
    d=ya
    ho=xa-x
    tmp = minloc(abs(x-xa))
    ns = tmp(1)
    y=ya(ns)
    ns=ns-1
    do m=1,n-1
       den(1:n-m)=ho(1:n-m)-ho(1+m:n)
       if (any(abs(den(1:n-m)) == 0.0)) then
          print *, 'polint: calculation failure'
          stop
       end if
       den(1:n-m)=(c(2:n-m+1)-d(1:n-m))/den(1:n-m)
       d(1:n-m)=ho(1+m:n)*den(1:n-m)
       c(1:n-m)=ho(1:n-m)*den(1:n-m)
       if (2*ns < n-m) then
          dy=c(ns+1)
       else
          dy=d(ns)
          ns=ns-1
       end if
       y=y+dy
    end do
  END SUBROUTINE polint

end program test
