import numpy as np
import matplotlib.pyplot as plt
from subprocess import PIPE, Popen


inputfile = """F  F  F                  	     :: suppress output to screen?, output dimensionless t?, log output?
%.7g T 1.0 1.0               :: initial saturated thickness, compute L/Le?, L, Le
0.001 1.0 1.0            :: robs, F, Ks
%.7g  %.7g              :: r, z observation locations     
%.7g  %.7g     	     :: l,d; depth to bottom, top of test interval
%.7g  %.7g     	     :: rw,rc; wellbore, tubing radius
%.7g  1.0D+0                 :: Kr,kappa
%.7g  %.7g         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
40  1.0D-8  1.0D-9           :: deHoog invlap;  M,alpha,tol
7  4                         :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
1  1  12  100                :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
-2  3  40                   :: logspace times; lo, hi, # times to compute
T  26  data.dat             :: compute times?, # times to read, filename for times (1 time/line)
data.out               """

Kr = 5.8E-3
Ss = 2.1E-4
Sy = 0.25

rw = 3.15E-2
rc = 1.55E-2

B = 5.81
l = 2.00
d = 1.95

rvec = [2.9] ##np.linspace(0.25,5.0,3)
colors = ['black','green','blue','red','pink','orange']

zvec = [2.63, 3.63, 5.63] ##np.linspace(0.0,B,4)
lines = ['-','--',':','.-']

fig = plt.figure(1,figsize=(25,10))
ax1 = fig.add_subplot(131)
ax2 = fig.add_subplot(132)
ax3 = fig.add_subplot(133)

for ln,r in zip(lines,rvec):
    for c,z in zip(colors,zvec):
        
        print 'r',r,' z',z
        fh = open('input-gen.dat','w')
        fh.write(inputfile % (B,r,z,l,d,rw,rc,Kr,Ss,Sy))
        fh.close()

        P = Popen(['./debug_genslug'],stdout=PIPE)
        stdout = P.communicate()[0]

        x = np.loadtxt('data.out',skiprows=15)

        ax1.loglog(x[:,0],x[:,1],color=c,linestyle=ln,marker=None)
        ax2.semilogx(x[:,0],x[:,1],color=c,linestyle=ln,marker=None)
        ax3.semilogx(x[:,0],x[:,2],color=c,linestyle=ln,marker=None,label='r=%.2g, z=%.2g'%(r,z))

ax3.legend(loc=0)
ax1.set_xlabel('time')
ax2.set_xlabel('time')
ax3.set_xlabel('time')
ax1.set_ylabel('$\\Phi_{uc}$')
ax1.set_ylabel('log(t) deriv')
plt.savefig('slug-observation-loc.png',dpi=200)
