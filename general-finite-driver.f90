!
! Copyright (c) 2011-2016 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

Program Driver

  ! constants and coefficients
  use constants, only : PI, DP, EP
  use inverse_Laplace_transform, only : INVLT
 
  use lap_hank_finite_soln, only : lhr => unconfined_finite_wellbore_slug
  use lap_hank_finite_soln_gen, only : lhrz => unconfined_finite_wellbore_slug_gen
  use inverse_Laplace_Transform, only : dehoog_invlap, dehoog_pvalues 

  use utilities, only : logspace

  implicit none

  real(DP) :: bD, lD, dD, rDw, CD, zD, rD
  real(DP) :: alphaD, kappa
  complex(EP), allocatable :: PHIuc(:), Hankel_result(:)

  integer :: layer
  type(INVLT) :: lap
  
  character(75) :: outfile, infile, timefname
  integer :: i, numt, np, minlogt,maxlogt
  
  ! vectors of results and intermediate steps
  real(DP), allocatable  :: ts(:), td(:), dt(:) 
  complex(EP), allocatable :: fpa(:,:)
  real(DP) :: tee

  ! Bessel function zeros and finite Hankel inversion kernel
  real(EP), allocatable :: j0(:), J0J1(:,:,:)
  integer :: Hsumord, n, err, numtcomp,numtfile, ierr,idx
  
  ! dimensional things
  logical :: quiet, dimless, computetimes, logHout, computeLe, computeOmega, accelerate, obsOscillation
  real(DP) :: B, Kr, Kz, sigma, Ss, Sy, r,z, Router, RoutD
  real(DP) :: l,d,LL,LLe,LLeo,LLo,rw,rc,g,nu,Tc,gamma, bb
  real(DP) :: robs, Cobs, omegao, gammao, delta_total, ravg
  real(DP), dimension(3) :: kv, deltav, delta_starv, rv
  real(EP), dimension(2) :: beta
  real(EP) :: head_result, deriv_result, omegaDo, gammaDo

  intrinsic :: get_command_argument, bessel_j0, bessel_j1

  ! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  ! read input parameters from file some minimal error checking
  call get_command_argument(1,infile)
  if (len_trim(infile) == 0) then
     infile = 'input-finite.dat'
  else
     ! change directory if infile has a slash in it             
     ! // other paths better not have paths in their filenames //
     idx = index(infile,'/',back=.true.) ! search from right (just get filename without path parts)
     if (idx /= 0) then
        call chdir(trim(infile(1:idx-1)),ierr)
        if (ierr /= 0) then
           print *, 'ERROR changing directory to "'//trim(infile(1:idx-1))//&
                & '" based on input filename given with slashes in name: "'//trim(infile)//'"'
           stop
        else
           ! reset filename to part without directories or beginning slash
           infile = infile(idx+1:)
        end if
     end if
  end if

  open(unit=19, file=infile, status='old', action='read',iostat=err)
  if(err /= 0) then
     write(*,'(A)') 'ERROR opening main input file '//trim(infile)//' for reading'
     stop
  end if
  
  ! suppress output to screen, write dimensionless output?
  read(19,*) quiet, dimless, logHout, accelerate, Router
  ! initial saturated aquifer thickness [L]
  read(19,*) B, computeLe, LL, LLe              
  ! observation well storage parameters (r_obs is radius of observation well)
  read(19,*) robs, computeOmega, omegao, gammao, LLeo, LLo, obsOscillation        
  ! observation distance and vertical position [L]
  read(19,*) r, z   
  ! distance from aquifer top to bottom & top of packer (l > d) [L]       
  read(19,*) l, d   
  ! well and casing radii [L]
  read(19,*) rw, rc        
  ! aquifer radial K in three zones [L^2/T], thickness of three zones
  read(19,*) kv(1:3), deltav(1:3), Kz     
  ! aquifer specific storage [1/L] and specific yield [-]
  read(19,*) Ss, Sy      
  ! grav accel [L/T^2], kinematic viscosity [L^2/T]
  read(19,*) g, nu          
  
  if (z < d) then
     ! above top of source screen
     layer = 1
  elseif (z <= l) then
     ! adjacent to source screen
     layer = 2
  else
     ! below bottom of source screen
     layer = 3
  end if     
  
  if (.not. quiet) then
     write(*,'(A,2(L1,1X))') 'quiet?, dimless output? ', quiet, dimless
     write(*,'(A,L1,1X,ES14.7)') 'accelerate Hankel sum?, outer domain radius ', &
          &accelerate, Router
     write(*,'(A,ES14.7)') 'B (initial aquier sat thickness):',B  
     write(*,'(A,L1,2(1X,ES14.7))') 'compute L/Le?, L, Le: ', computeLe, LL, LLe
     write(*,'(A,ES14.7,1X,L1,4(1X,ES14.7))') 'Obs well related: r_obs (obs well radius), compute '//&
          &'Omega_o/gamma_o?, omega_o, gamma_o, LLeo, LLo:',robs, computeOmega, omegao, gammao, LLeo, LLo
     write(*,'(A,2(ES14.7,1X),A,I0)') 'observation point coordinates: r,z (z down from top)',r,z,&
          &' layer:',layer
     write(*,'(A,2(ES14.7,1X))') 'l,d (packer bot & top from top of aquifer):',l,d  
     write(*,'(A,2(ES14.7,1X))') 'rw,rc (well and casing radii):',rw,rc  
     write(*,'(A,2(ES14.7,1X))') 'Ss,Sy',Ss, Sy
     write(*,'(A,2(ES14.7,1X))') 'g,nu (grav acc & kin viscosity in consistent units)',g,nu 
  end if

  if (z < 0 .or. z > B) then 
     stop 'error: z<0 or z>b'
  end if
  
  if (r < rw) then
     print *, 'WARNING: resetting r -> rw',rw
     r = rw
  elseif (.not. quiet .and. abs(r-rw) < 0.1) then
     print *, '******************************************************************'
     print *, 'WARNING, not optimized for solution near source wellscreen (r~rw)'
     print *, '******************************************************************'
  end if

  if (r >= Router) then
     stop 'error: distance to observation well greater than or equal to domain size (r >= R_outer)'
  end if

  if (r > 2.0*Router) then
     print *, 'WARNING: distance to observation well (',r,') is close to domain size (',Router,')'//&
          &' you probably want to increase domain size to minimize boundary effects at observation well'
  end if
  
  read(19,*) lap%M, lap%alpha, lap%tol, Hsumord

  if (.not. quiet) then
     write(*,'(A,I0)') 'observation point in layer ',layer
  end if
  
  if(any([B,Kv,deltav,Ss,l,rw,rc,g,nu] <= 0.0)) then
     write(*,'(A)') 'input error: zero or negative distance or aquifer parameters' 
     stop
  end if

  if (abs(sum(deltav) - r) > 0.01) then
     write(*,'(A)') '****************************************'
     write(*,'(A,3(ES12.5,1X),A,ES12.5)') 'WARNING: sum(delta(1:3)) =/= r. delta=',deltav,' r=',r
     write(*,'(A)') '****************************************'
  end if
  
  if (d >= l) then
     write(*,'(2(A,ES12.5))') 'input error: l must be > d; l=',l,' d=',d
     stop
  end if

  if (lap%M < 1) then
     write(*,'(A,I0)')  'deHoog # FS terms must be > 0 M=',lap%M
     stop 
  end if

  if (lap%tol < epsilon(lap%tol)) then
     lap%tol = epsilon(lap%tol)
     write(*,'(A,ES12.5)') 'WARNING: increased INVLAP solution tolerance to ',lap%tol 
  end if

  if (Hsumord < 100) then
     print *, 'ERROR: number of terms in finite Hankel '//&
          &'inverse-transform sum must be >= 100 ',Hsumord
     print *, 'often this should be >= 1000'
     stop
  end if

  
  read(19,*) minlogt,maxlogt,numtcomp           ! log_10(t_min), log_10(t_max), # times
  read(19,*) computetimes,numtfile,timefname    ! compute times?, # times in file, time file  
  read(19,*) outfile                            ! output filename
  
  if (computetimes) then
     numt = numtcomp
  else
     numt = numtfile
  end if
  
  ! solution vectors
  np = 2*lap%M+1  
  allocate(lap%p(np), ts(numt), td(numt), dt(numt), PHIuc(np), Hankel_result(np), &
       & J0(Hsumord), J0J1(Hsumord,np,2), fpa(Hsumord,np))

  if (computetimes) then
     ! computing times 
     ts = logspace(minlogt,maxlogt,numtcomp)
  else
     open(unit=22, file=timefname, status='old', action='read',iostat=err)
     if(err /= 0) then
        write(*,'(A)') 'ERROR opening time data input file '//trim(timefname)//&
             &' for reading'
        stop 
     else
        ! times listed one per line
        do i=1,numtfile
           read(22,*,iostat=err) ts(i)
           if(err /= 0) then
              write(*,'(A,I0,A)') 'ERROR reading time ',i,&
                   &' from input file '//trim(timefname)
              stop
           elseif (ts(i) <= 0.0) then
              write(*,'(A,I0,A,ES12.5)') 'ERROR with time ',i,&
                   &' read from input file: t <= 0 ::',ts(i)
           end if
        end do
     end if
     close(22)
  end if
  
  ! compute Kr (in series)
  rv(1) = rw + deltav(1)/2.0
  rv(2) = rw + deltav(1) + deltav(2)/2.0
  rv(3) = rw + sum(deltav(1:2)) + deltav(3)/2.0
  ravg = (rv(1) + rv(3))/2.0
  delta_starv = (ravg/rv)*deltav
  delta_total = sum(deltav)
  Kr = delta_total/sum(delta_starv/kv)

  ! compute Kz (in parallel) <-- this overwrites Kz read in from input file (which wasn't used)
  Kz = sum(deltav*kv)/delta_total

  ! anisotropy ratio
  kappa = Kz/Kr 

  ! compute derived or dimensionless properties
  gamma = 1.0_DP   ! no skin, always 1
  sigma = Ss*B/Sy
  alphaD = kappa*sigma
  rDw = rw/B
  bb = l-d
  bD = bb/B
  lD = l/B
  dD = d/B
  rD = r/B
  zD = z/B
  Tc = B**2/(Kr/Ss)
  RoutD = Router/B
  if (computeLe) then
     ! overwrite values read in from input file
     LL =    d + bb/2.0_DP*(rc/rw)**4
     LLe =  LL + bb/2.0_DP*(rc/rw)**2
  end if
  if (computeOmega) then
     ! overwrite values read in from input file
     omegao = sqrt(g/LLeo)
     gammao = 8.0_DP*nu*LLo/(rc**2 *LLeo)
  end if

  beta(1) = 8.0_EP*nu*LL/(rc**2 *g*Tc)
  beta(2) = LLe/(g*Tc**2)
  CD = (rc/B)**2/(Ss*bb)
  td(1:numt) = ts(:)/Tc

  omegaDo = omegao*Tc
  gammaDo = gammao*Tc

  Cobs = PI*robs**2

  ! compute zeros of Bessel function J_0(RoutD*x)
  ! which are the roots appropriate for the finite hankel
  ! transform with a homogeneous dirichlet condition at RoutD
  j0(1:Hsumord) = besselzeros(Hsumord)/RoutD

  !! open(unit=77,file='bessel.zeros',action='write',status='replace')
  !! do n=1,Hsumord
  !!    write(77,*) n,j0(n),bessel_j0(j0(n))
  !! end do
  !! close(77)
    
  if (.not. quiet) then
     write(*,'(A,3(ES14.7,1X))') 'K_{1-3}:  ',kv
     write(*,'(A,3(ES14.7,1X))') 'delta_{1-3}:  ',deltav
     write(*,'(A,2(ES14.7,1X))') 'computed Kr, Kz:   ',kr, kz
     write(*,'(A,ES14.7)') 'computed kappa:   ',kappa
     write(*,'(A,ES14.7)') 'sigma:   ',sigma
     write(*,'(A,ES14.7)') 'alpha_D: ',alphaD
     write(*,'(A,ES14.7)') 'r_{D,w}: ',rDw
     write(*,'(A,ES14.7)') 'R_{D,outer}: ',RoutD
     write(*,'(A,ES14.7)') 'b_D: ',bD
     write(*,'(A,ES14.7)') 'l_D: ',lD
     write(*,'(A,ES14.7)') 'Tc:  ',Tc
     write(*,'(A,L1)') 'compute L/Le?: ',computeLe
     write(*,'(A,ES14.7)') 'L:   ',LL
     write(*,'(A,ES14.7)') 'Le:  ',LLe
     write(*,'(A,L1)') 'compute omega_o/gamma_o?: ',computeOmega
     write(*,'(A,2(ES14.7,1X))') 'omega_Do, gamma_Do:  ',omegaDo,gammaDo
     write(*,'(A,1(ES14.7,1X))') 'beta: ',beta
     write(*,'(A,ES14.7)') 'C_D: ',CD
     write(*,'(A,I0,2(ES14.7,1X))') 'deHoog: M,alpha,tol: ',lap%M, lap%alpha, lap%tol
     write(*,'(A,I0)') '# finite Hankel transform terms: ',Hsumord
     write(*,'(A,L1)') 'compute times? ',computetimes
     if(computetimes) then
        write(*,'(A,3(I0,1X))') 'log10(tmin), log10(tmax), num times ',minlogt,maxlogt,numtcomp
     else
        write(*,'(A,I0,1X,A)') 'num times, filename for t inputs ',numtfile,trim(timefname)
     end if
  end if

  ! open output file or die
  open (unit=20, file=outfile, status='replace', action='write', iostat=err)
  if (err /= 0) then
     print *, 'cannot open output file for writing' 
     stop
  end if
  
  ! echo input parameters at head of output file
  write(20,'(A,5(L1,1X))') '# quiet? , write log10(abs(H))?, accelerate H?, compute L/Le?, '//&
       &'compute omega_o/gamma_o?: ', quiet, logHout, accelerate, computeLe, computeOmega
  write(20,'(A,ES14.7)') '# B (aquier sat thickness): ',B  
  write(20,'(A,2(ES14.7,1X),A,I0)') '# observation loc: r,z (z down from top)',r,z,&
       &' "layer" in partial-penetration system:',layer
  write(20,'(A,2(ES14.7,1X))') '# l,d (packer bot & top from top of aquifer): ',l,d  
  write(20,'(A,3(ES14.7,1X))') '# rw,rc,Router (well, casing, and domain radii): ',rw,rc,Router  
  write(20,'(2(A,3(ES14.7,1X)))') '# K_{1-3}: ',kv, ' delta_{1-3}:',deltav
  write(20,'(A,3(ES14.7,1X))') '# (computed) Kr,kappa,Kz: ',Kr, kappa, Kz
  write(20,'(A,1(ES14.7,1X))') '# (computed) R_{D,outer}: ',RoutD
  write(20,'(A,2(ES14.7,1X))') '# Ss,Sy: ',Ss, Sy
  write(20,'(A,5(ES14.7,1X))') '# Cobs, omegaDo, gammaDo, Leo, Lo: ',Cobs,omegaDo,gammaDo,LLeo,LLo
  write(20,'(A,2(ES14.7,1X))') '# g,nu (grav acc & kin viscosity in consistent units): ',g,nu 
  write(20,'(A,I0,2(ES14.7,1X))') '# deHoog M, alpha, tol: ',lap%M,lap%alpha,lap%tol
  write(20,'(A,I0)'), '# finite Hankel terms: ',Hsumord
  write(20,'(A,L1)') '# compute times?: ',computetimes
  if(computetimes) then
     write(20,'(A,3(I0,1X))') '# log10(tmin), log10(tmax), num times: ',minlogt,maxlogt,numtcomp
  else
     write(20,'(A,I0,1X,A)') '# num times, filename for t inputs: ',numtfile,trim(timefname)
  end if
    if (dimless) then
     write (20,'(A,/,A,/,A)') '#',&
          & '#         t_D               H^{-1}[ L^{-1}[ f_D ]]    deriv wrt log t',&
          & '#---------------------------------------------------------------------------'
  else
     write (20,'(A,/,A,/,A)') '#',&
          &'#         t                   H^{-1}[ L^{-1}[ f ]]     deriv wrt log t',&
          & '#---------------------------------------------------------------------------'
  end if

  ! these terms in the finite Hankel inversion don't change with p

  ! Hankel back-transform evaluated at the source well screen (rDw)
  J0J1(1:Hsumord,1:np,1) = 2.0_EP/(RoutD**2)* &
       & spread(bessel_j0(j0(:)*rDw)/(bessel_j1(j0(:)*RoutD)**2),2,np)

  ! Hankel back-transform evalutated at observation location (rD)
  J0J1(1:Hsumord,1:np,2) = 2.0_EP/(RoutD**2)* &
       & spread(bessel_j0(j0(:)*rD)/(bessel_j1(j0(:)*RoutD)**2),2,np)
  
  ! loop over all requested times
  do i = 1, numt
     if (.not. quiet) then
        write(*,'(I4,A,ES11.4)') i,' td ',td(i)
     end if
     tee = td(i)*2.0  ! good guess at de Hoog scaling factor
     lap%p = dehoog_pvalues(tee,lap)

     ! compute solution at densest set of abcissa
     !$OMP PARALLEL DO PRIVATE(n) SHARED(fpa)
     do n=1,Hsumord
        fpa(n,1:np) = lhr(j0(n),bD,dD,lD,alphaD,rDw,CD,kappa,lap)
     end do
     !$OMP END PARALLEL DO

     if (accelerate) then
        !$OMP PARALLEL DO PRIVATE(n) SHARED(Hankel_result)
        do n=1,np
           Hankel_result(n) = wynn_epsilon(fpa(:,n)*J0J1(:,n,1))
        end do
        !$OMP END PARALLEL DO
     else
        Hankel_result(1:np) = sum(fpa(:,:)*J0J1(:,:,1),dim=1)
     end if
     
     Hankel_result(1:np) = beta(1) + beta(2)*lap%p(:) + gamma*Hankel_result(:)/2.0_EP ! f
     PHIuc(1:np) = Hankel_result(:)/(1.0_EP + lap%p(:)*Hankel_result(:))  ! Phi_uc

     ! @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

     ! compute solution at densest set of abcissa
     !$OMP PARALLEL DO PRIVATE(n) SHARED(fpa)
     do n=1,Hsumord
        fpa(n,1:np) = lhrz(j0(n),dD,lD,zD,alphaD,rDw,CD,kappa,lap,PHIuc,layer)
     end do
     !$OMP END PARALLEL DO

     if (accelerate) then
        !$OMP PARALLEL DO PRIVATE(n) SHARED(Hankel_result)
        do n=1,np
           Hankel_result(n) = wynn_epsilon(fpa(:,n)*J0J1(:,n,2))
        end do
        !$OMP END PARALLEL DO
     else
        Hankel_result(1:np) = sum(fpa(:,:)*J0J1(:,:,2),dim=1)
     end if

     ! apply wellbore storage to monitoring point
     ! s_Dobs = f2(p)*<s_D>
     if (obsOscillation) then
       Hankel_result = Hankel_result*omegaDo**2/(lap%p**2 + lap%p*gammaDo + omegaDo**2)
     end if
     head_result = dehoog_invlap(td(i),tee,Hankel_result,lap)

     ! time derivative is multiplication by p
     Hankel_result = Hankel_result*lap%p
     deriv_result = td(i)*dehoog_invlap(td(i),tee,Hankel_result,lap)

     ! write results to file (as we go)
     if (logHout) then
        if (dimless) then
           write (20,'(3(1x,ES24.15E3))') td(i), log10(abs(head_result)), deriv_result
        else
           write (20,'(3(1x,ES24.15E3))') ts(i), log10(abs(head_result)), deriv_result
        end if
     else
        if (dimless) then
           write (20,'(3(1x,ES24.15E3))') td(i), head_result, deriv_result
        else
           write (20,'(3(1x,ES24.15E3))') ts(i), head_result, deriv_result
        end if
     end if
     if (i == numt) then
        !$OMP BARRIER
        close(20)
        deallocate(Hankel_result,fpa,ts,td,dt,PHIuc)
     end if
  end do

  ! this ~empty file written as a signal to know program done
  ! needed by parallel DREAM implementation
  open(unit=50,file=trim(infile)//'.DONE',status='replace',action='write')
  write(50,'(/)')
  close(50)

contains

  !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  
  !!   end of program flow
  !! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  

  function besselzeros(n) result(j0)
    use constants, only : EP, PIEP
    implicit none
    integer, intent(in) :: n
    real(EP), dimension(n) :: j0

    integer :: i
    real(EP) :: x,dx

    intrinsic :: bessel_j0, bessel_j1

    ! ## compute zeros of J0 bessel function #####
    ! asymptotic estimate of zeros - initial guess
    forall (i=0:n-1)
       j0(i+1) = (i + 0.75)*PIEP
    end forall
    do i=1,n
       x = j0(i)
       NR: do
          ! Newton-Raphson
          dx = bessel_j0(x)/bessel_j1(x)
          x = x + dx
          if(abs(dx) < spacing(x)) then
             exit NR
          end if
       end do NR
       j0(i) = x
    end do
  end function besselzeros

  !! ###################################################
  !! wynn-epsilon acceleration of partial sums, given a series
  !! all intermediate sums / differences are done in extended precision
  pure function wynn_epsilon(series) result(accsum)
    use constants, only : EP
    implicit none

    integer, parameter :: MINTERMS = 4
    real(EP), parameter :: EXPONE = exponent(1.0_EP)
    real(EP), parameter :: MAXEXPONE = maxexponent(1.0_EP)
    real(EP), parameter :: SPC = spacing(0.0_EP)
    complex(EP), dimension(:), intent(in) :: series
    complex(EP) :: accsum, denom
    integer :: ns, i, j, m
    complex(EP), dimension(1:size(series),-1:size(series)-1) :: eps

    ns = size(series)

    ! build up partial sums, but check for problems
    check: do i=1,ns
       if (.not. is_finite(series(i))) then
          ns = i-1
          if(ns < MINTERMS) then
             accsum = -999999.9  ! make it clear answer is bogus
             goto 777
          else
             exit check
          end if
       else
          ! term is good, continue
          eps(i,0) = sum(series(1:i))
       end if
    end do check

    ! first column is intiallized to zero
    eps(:,-1) = 0.0_EP

    ! build up epsilon table (each column has one less entry)
    do j = 0,ns-2
       do m = 1,ns-(j+1)
          denom = eps(m+1,j) - eps(m,j)
          ! check for overflow and div by zero
          if(EXPONE - exponent(abs(denom)) >= MAXEXPONE .or. abs(denom) > SPC) then 
             eps(m,j+1) = eps(m+1,j-1) + 1.0_EP/denom
          else
             accsum = eps(m+1,j)
             goto 777
          end if
       end do
    end do

    ! if made all the way through table use "corner value" of triangle as answer
    if(mod(ns,2) == 0) then
       accsum = eps(2,ns-2)  ! even number of terms - corner is acclerated value
    else
       accsum = eps(2,ns-3)  ! odd numbers, use one column in from corner
    end if
777 continue

  end function wynn_epsilon

  elemental function is_finite(x) result(pred)
    use constants, only : EP
    complex(EP), intent(in) :: x
    logical :: pred
    intrinsic :: isnan ! gfortran extension
    pred = .not. (isnan(abs(x)) .or. abs(x) > huge(abs(x)))
  end function is_finite
end program Driver
