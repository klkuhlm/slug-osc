!
! Copyright (c) 2011 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

module constants

  ! real with range 300 orders of mag, 15 sig figs (8 on both g95 & ifort)
  integer, parameter :: DP = selected_real_kind (p=15,r=300)

  !! full quad precision (only on gfortran >= 4.6 and ifort)
  !integer, parameter :: QP = selected_real_kind(p=33,r=3000)

  !! extended range internal variables (10 on g95, 10 on gfortran, 16 on ifort)
  integer, parameter :: EP = selected_real_kind(r=3000)
  
  !! 3.141592653589793238462643383279503_EP
  real(DP), parameter :: PI =    4.0_DP*atan(1.0_DP) 
  real(EP), parameter :: PIEP =  4.0_EP*atan(1.0_EP)
 
  real(DP), parameter :: PIOV2 = 2.0_DP*atan(1.0_DP)
  real(EP), parameter :: PIOV2EP = 2.0_EP*atan(1.0_EP)

  !! 0.693147180559945309417232121458177_EP
  real(EP), parameter :: LN2EP = log(2.0_EP) 

  real(EP), parameter :: EONE = 1.0_EP

end module constants
