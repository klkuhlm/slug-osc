!
! Copyright (c) 2011-2015 Kristopher L. Kuhlman (klkuhlm at sandia dot gov)
! 
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.
!

!! this file has _two_ modules in it 

!! ######################################################################
!! ######################################################################

module shared_data
  use constants, only : DP, EP
  use inverse_Laplace_transform, only : INVLT
  implicit none

  ! variables to pass using modules

  ! parameters to change between calculations
  real(DP) :: tsval  ! time - passed through hankel integral to Lap xform

  ! aquifer parameters (secondary - computed from above)
  real(DP) :: bD, lD, dD, rDw, CD, zD, rD
  real(DP) :: alphaD, kappa
  complex(EP), allocatable :: PHIuc(:)

  integer :: layer
  type(INVLT) :: lap

end module shared_data

!! ######################################################################
!! ######################################################################

module utilities
implicit none
private
public :: ccosh, csinh, logspace

contains
  pure elemental function ccosh(z) result(f)
    use constants, only : EP
    complex(EP), intent(in) :: z
    complex(EP) :: f
    real(EP) :: x,y
    x = real(z)
    y = aimag(z)
    f = cmplx(cosh(x)*cos(y), sinh(x)*sin(y),EP)
  end function ccosh
  
  pure elemental function csinh(z) result(f)
    use constants, only : EP
    complex(EP), intent(in) :: z
    complex(EP) :: f
    real(EP) :: x,y
    x = real(z)
    y = aimag(z)
    f = cmplx(sinh(x)*cos(y), cosh(x)*sin(y),EP)
  end function csinh
  
  pure function linspace(lo,hi,num) result(v)
    use constants, only : DP
    real(DP), intent(in) :: lo,hi
    integer, intent(in) :: num
    real(DP), dimension(num) :: v
    integer :: i
    real(DP) :: dx
    
    if (num == 1) then
       v = [lo]
    else
       dx = (hi-lo)/(num-1)
       forall (i=1:num) 
          v(i) = lo + (i-1)*dx
       end forall
    end if
  end function linspace

  pure function logspace(lo,hi,num) result(v)
    use constants, only : DP
    integer, intent(in) :: lo,hi,num
    real(DP), dimension(num) :: v
    v = 10.0_DP**linspace(real(lo,DP),real(hi,DP),num)
  end function logspace

end module utilities


