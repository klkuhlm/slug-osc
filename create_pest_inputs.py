from sys import argv, exit

# read in datafile with times and heads
# one observation per line, space delimited

try:  
    datafname = argv[1]
    fin = open(datafname,'r')
except:
    print 'ERROR: supply datafile with observed times & heads on command line.', \
	'Unable to open file',datafname
    exit(1)

# assume datafile ends in '.dat' extension, created files will use that prefix elsewhere
prefix = datafname.replace('.dat','')
outfname = prefix + '.out'

# for comma-delimited data, change .split() in the next line to .split(',')
data = [line.strip().split() for line in fin.readlines()]
dataval = [[float(l[0].strip()),float(l[1].strip())] for l in data]
del data
nobs = len(dataval)

## pest instruction file reads output from Fortran code
ins = open(prefix+'.ins','w')
ins.write('pif @\n') # pest-required header
ins.write('l15 \n') # skip header in model output file
for i,line in enumerate(dataval):
        ins.write("l1 [obs%4.4i]28:50\n" % (i+1,))
ins.close()

##############################
## pest template file 
ptf = open(prefix+'.ptf','w')
ptf.write("""ptf @
T  T                	     :: suppress output to screen?, write dimensionless time?
@     b      @               :: initial saturated thickness
1.93D+1  1.90D+1     	     :: l,d; depth to bottom, top of test interval
2.50D-2  1.30D-2     	     :: rw,rc; wellbore, tubing radius
@     Kr     @  @   kappa   @         :: Kr,kappa
@     Ss     @  @    Sy     @         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
40  1.0D-8  1.0D-9           :: deHoog invlap;  M,alpha,tol
7  4                         :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
1  1  12  100                :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
-2  4  400                   :: logspace times; lo, hi, # times to compute
F  %i  %s             :: compute times?, # times to read, filename for times (1 time/line)
%s
""" % (nobs,datafname,outfname))
ptf.close()

###################
## pest parameter file (to allow pest utils to write test input file)
params = {'b': {'lo':10.0,    'hi':30.0, 'init':20.0,      'trans':'fixed'}, 
	  'Kr': {'lo':1.0E-8, 'hi':1.0E+4, 'init':8.0E-4, 'trans':'log'},
	  'kappa': {'lo':1.0E-8, 'hi':1.0, 'init':1.0,    'trans':'log'},
	  'Ss': {'lo':1.0E-8, 'hi':0.1,    'init':2.0E-4, 'trans':'log'},
	  'Sy': {'lo':1.0E-4, 'hi':0.75,    'init':0.25,   'trans':'log'}}

par = open(prefix+'.par','w')
par.write('double  point\n')
for key in params.keys():
	par.write('%s  %.2f  1.0  0.0\n' % (key,params[key]['init']))
par.close()

####################
## pest control file
pcf = open(prefix+'.pst','w')

pcf.write("""pcf
* control data
restart estimation
%i %i 1 0 1
1 1 double point 1 0 0
5.0 2.0 0.4 0.001 10
3.0 3.0 1.0E-3
0.1
30 0.01 3 3 0.01 3
1 1 1
* parameter groups
pargrp relative 0.005 0.0001 switch 2.0 parabolic
""" % (len(params),nobs))

pcf.write('* parameter data\n')
for key in params.keys():
	pcf.write('%s  %s  factor  %.5e  %.5e  %.5e  pargrp  1.0  0.0  1\n' % 
		(key, params[key]['trans'], params[key]['init'], 
                 params[key]['lo'], params[key]['hi']))

pcf.write("""* observation groups
norm_head
* observation data
""")

# assume all observations have a weight of 1
for i,val in enumerate(dataval):
	pcf.write('obs%4.4i  %.7e  1.0   norm_head\n' % (i+1,val[1]))

pcf.write("""* model command line
./slug
* model input/output
%s.ptf input.dat
%s.ins %s
""" % (prefix,prefix,outfname))
pcf.close()
