import numpy as np
from scipy.linalg import lu
import matplotlib.pyplot as plt
import matplotlib.colors as mpc
from matplotlib import cm

norm = mpc.Normalize(vmin=-9,vmax=-2)

for i in [1,3,5,7,9]:

    # skip header row and observation label column
    J = np.loadtxt('test1obs_jco-finite-P13-MC1-%i.txt' % i, skiprows=1, usecols=(1,2,3,4,5,6,7))

    # use LU decomposition with permutation (sort of like Gaussian elimination)
    pl,u = lu(J, permute_l=True)

    fig = plt.figure(1)
    ax = fig.add_subplot(111)

    cax = ax.imshow(np.log10(np.abs(u)),interpolation='none',norm=norm)
    cb = fig.colorbar(cax)
    cb.ax.set_ylabel('$\\log_{10}(|U|)$')
    ax.set_title('LU decomposition of $J$ for P13-MC1-%i' % i)
    
    plt.savefig('jacobian-linear-independence-%i.png' % i)
    plt.close(1)

    
