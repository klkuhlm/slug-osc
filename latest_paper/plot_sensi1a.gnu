unset label
unset arrow
unset logscale
unset key
set xrange [0:20];
set yrange [-8:4];
set lmargin 8
set bmargin 4

#set format x "10^{%T}"
#set format y "10^{%T}"
#set logscale x;
#set logscale y;
#set xtics 1e-2,1e2,1e10;
#set ytics -2,1,2;

set xlabel "Time, t (s)" font "Helvetica,30"
set ylabel "Sensitivity\t\tJ_{{/Symbol q}_m}\t\t ({/Symbol \264} 10^{-3})" font "Helvetica,28" offset 0
set key horiz bottom right spacing 1.75 font "Helvetica,26"

#set label "P13-MC1-1" at graph 0.7,0.95
set label "(a)" at graph 0.05,0.95

set style line 1 lt 2 lc rgb "red" lw 3
set style line 2 lt 2 lc rgb "orange" lw 4
set style line 3 lt 2 lc rgb "yellow" lw 3
set style line 4 lt 2 lc rgb "green" lw 4

#teps = 0.0;
##t(s) ll  le  ss  k1  kr  sy  leo  llo
#ll = 0.692464
#le =  5.70037
#ss = 1.978425E-05
#k1 = 0.169378
#kr = 6.183109E-04
#sy = 4.071032E-02
#leo= 1.867903E-02
#llo=  4.06623

set size 1.2,1.3
#set termoption dash
set output 'p13mc1-1sen1.eps'
set xzeroaxis
set xrange [0:18]
set y2range [-0.01:0.025]
set yrange [-7:5]
set term postscript eps enhanced 'Helvetica' 28 color
plot 'test1obs_jco-1-times.txt' u 1:(1e3*$6) t "K"        w l lw 4 axes x1y1, \
     'test1obs_jco-1-times.txt' u 1:(1e3*$5) t "K_{skin}" w l lw 4 axes x1y1, \
     'test1obs_jco-1-times.txt' u 1:(1e3*$4) t "S_s" 	   w l lw 4 axes x1y1, \
     'test1obs_jco-1-times.txt' u 1:(1e3*$7) t "S_y" 	   w l lw 4 axes x1y1, \
     '../swiss-data/obs_well-P13-MC1-1.tsv' u (0.63+$1):2 t 'obs' w p lt 1 axes x1y2,\
     0.0 t "" lw 1 lc 'black' axes x1y2

unset label
#set ylabel offset 2,0
#set lmargin 10
set ylabel "Sensitivity\t\t|J_{{/Symbol q}_m}|" font "Helvetica,28"
set key horiz top right
set label "(b)" at graph 0.05,0.95
set output 'p13mc1-1sen1-log.eps'
set logscale y
set format y "10^{%L}"
set yrange [0.00001:0.01]
plot 'test1obs_jco-1-times.txt' u 1:(abs($6)) t "K"        w l lw 4, \
     'test1obs_jco-1-times.txt' u 1:(abs($5)) t "K_{skin}" w l lw 4, \
     'test1obs_jco-1-times.txt' u 1:(abs($4)) t "S_s" 	    w l lw 4, \
     'test1obs_jco-1-times.txt' u 1:(abs($7)) t "S_y" 	    w l lw 4
