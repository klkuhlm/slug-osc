\documentclass[12pt,letterpaper]{article}
\usepackage{epsfig,natbib,amssymb,lineno,amsfonts,setspace,fullpage}
\usepackage{color}
\usepackage{amsmath,amssymb}
\usepackage{bm}

\title{Effective Hydraulic Conductivity Equations for Series Arrangement in Axial-symmetric System}\date{}
\begin{document}
\maketitle

To obtain an approximate relation that allows one to account for source wellbore skin, consider the series arrangement of the source well and the formation as illustrated in Figure \ref{fig:wellskin}. The source well of radius $r_w$ has a skin of radial extent $r_\mathrm{skin}$ from the center of the source well, with a thickness $\delta_1=r_\mathrm{skin}-r_w$. We neglect observation well skin in this case, and the observation well is simply a point in the formation at a radial distance $r_\mathrm{obs}$ from the source well. The thickness of the formation between source well skin and the observation well is $\delta_2=r_\mathrm{obs}-r_w$. Let $h_1$ be the head in the well, $h_2$ the head at the edge of source well skin, and $h_3$ he the head at the observation well. Under steady state conditions with radially convergent flow, it follows from a direct application of Darcy's law and mass conservation in the radial direction that
  \begin{align}
  Q & = 2\pi b r_1 K_1 \frac{h_2-h_1}{\delta_1}\label{eq:skin-flow}\\
    & = 2\pi b r_2 K_2 \frac{h_3-h_2}{\delta_2}\\
    & = 2\pi b \hat{r} K_\mathrm{eff}\frac{h_3-h_1}{\delta_T}\label{eq:eff-flow}
  \end{align}
where $r_1 = (r_w+r_\mathrm{skin})/2$, $r_2 = (r_\mathrm{skin} + r_\mathrm{obs})/2$, $\hat{r} = (r_w + r_\mathrm{obs})/2$, $K_1=K_\mathrm{skin}$, $K_2$ is formation hydraulic conductivity, and $b$ is formation thickness. Equations (\ref{eq:skin-flow}-\ref{eq:eff-flow}) are based on the simplifying assumption of a piecewise linear head distribution in the skin and formation. The actual (pseudo)steady-state head distribution is given by the Thiem equation; the simplifying assumption adopted here approximates the function $h\sim \log(r)$ with two straight lines as depicted in Figure \ref{fig:wellskin} and yields a centered finite difference approximation of the flow, i.e., if one uses the Theim equation and a centered difference approximation of the hydraulic gradient at $r_1$ and $r_2$, the result is the same as that given in equations (\ref{eq:skin-flow}-\ref{eq:eff-flow}).
  
Eliminating $h_2$ from these equations and reducing leads to
  \begin{equation}
  \frac{\delta_T}{\hat{r}K_\mathrm{eff}} = \frac{\delta_1}{r_1 K_1} + \frac{\delta_2}{r_2 K_2}
  \end{equation}
where $\delta_T=\delta_1+\delta_2$. This equation can be rewritten as
  \begin{equation}
  \label{eq:wellskin}
  K_\mathrm{eff} = \delta_T\left(\frac{\delta_1^\ast}{K_1} + \frac{\delta_2^\ast}{K_2} \right)^{-1}
  \end{equation}
where $\delta_1^\ast= (\hat{r}/r_1)\delta_1$ and $\delta_2^\ast= (\hat{r}/r_2)\delta_2$. This is simply the equation of the harmonic mean with weights being functions of the radial skin dimensions and distance to the observation well. It can be extended using inductive reasoning to
  \begin{equation}
  K_\mathrm{eff} = \delta_T \left(\sum_{n=1}^N\frac{\delta_n^\ast}{K_n}\right)^{-1}
  \end{equation}
for $N$ concentric cylinders centered about the source well.
  
Similarly, applying mass conservation and Darcy's law to the vertical direction leads to
  \begin{align}
  Q_1 & = \pi (r_\mathrm{skin}^2 - r_w^2) K_1 \frac{\Delta h}{b} \equiv 2\pi r_1 \delta_1 K_1 \frac{\Delta h}{b}\\
  Q_2 & = \pi (r_\mathrm{obs}^2-r_\mathrm{skin}^2) K_2 \frac{\Delta h}{b} \equiv 2\pi r_2 \delta_2 K_2 \frac{\Delta h}{b}\\
  Q_T & = \pi (r_\mathrm{obs}^2-r_w^2) K_\mathrm{eff} \frac{\Delta h}{b} \equiv 2\pi \hat{r} \delta_T K_\mathrm{eff} \frac{\Delta h}{b}
  \end{align}
where $Q_1$ and $Q_2$ are the vertical volume flow rates in the skin and formation cylindrical layer bounded by the observation well, and $Q_T=Q_1+Q_2$. It follows then that
  \begin{equation}
  K_\mathrm{eff,v} = \frac{1}{\delta_T}\left(\delta_1 K_1 + \delta_2 K_2\right),
  \end{equation}
which extends to
  \begin{equation}
  K_\mathrm{eff,v} = \frac{1}{\delta_T}\sum_n^N\delta_n K_n,
  \end{equation}
for $N$ concentric cylinders centered about the source well.
  
These are the equations used for the source well skin approximation. It is emphasized here that only the radial (series) equation is different; the equation for flow parallel to the axis of the concentric cylinders is the same as the case for flat layers. This is a first-order attempt at including skin in the computations. We neglect observation well bore skin as it is not concentric with the source well. Observation wellbore skin effects are now part of the formation hydraulic properties. The use of steady-state radial and vertical flow to derive the effective hydraulic conductivities does not imply that flow during the slug tests is steady-state. It only provides a vehicle for obtaining first-order approximate expressions for source well skin. An exact expression of the head distribution in the formation accounting for wellbore skin in slug tests is described in Malama et al. (2011) for the case of a constant head boundary condition at the water table. The derivation of the exact solution for a kinematic water table boundary condition is a non-trivial exercise and is outside the scope of the present work, hence the use of this approximate relation. The accuracy of the approximation for the effective radial hydraulic conductivity is simply the accuracy of the finite difference approximation to the Thiem equation.

  \begin{figure}
  \centering \includegraphics[scale=0.45]{wellskin} \includegraphics[scale=0.45]{approx}
  \caption{Schematic for source well skin approximation for radial system.\label{fig:wellskin}}
  \end{figure}
\end{document}