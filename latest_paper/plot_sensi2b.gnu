unset label
unset arrow
unset logscale
unset key
set xrange [0:18];
set yrange [-1.7:1.25];
set lmargin 8
set bmargin 4

#set format x "10^{%T}"
#set format y "10^{%T}"
#set logscale x;
#set logscale y;
#set xtics 1e-2,1e2,1e10;
set ytics -2,1,3;

set xlabel "Time, t (s)" font "Helvetica,30"
set ylabel "Sensitivity\t\tJ_{{/Symbol q}_m}" font "Helvetica,28" offset 1
set key horiz bottom right spacing 1.5 font "Helvetica,24"

#set label "P13-MC1-1" at graph 0.7,0.95
set label "(c)" at graph 0.05,0.95

set style line 1 lt 2 lc rgb "red" lw 3
set style line 2 lt 2 lc rgb "orange" lw 4
set style line 3 lt 2 lc rgb "yellow" lw 3
set style line 4 lt 2 lc rgb "green" lw 4

#teps = 0.0;
##t(s) ll  le  ss  k1  kr  sy  leo  llo
#ll = 0.692464
#le =  5.70037
#ss = 1.978425E-05
#k1 = 0.169378
#kr = 6.183109E-04
#sy = 4.071032E-02
#leo= 1.867903E-02
#llo=  4.06623

set size 1.2,1.3
#set termoption dash
set xzeroaxis
set output 'p13mc1-5sen2.eps'
set term postscript eps enhanced 'Helvetica' 28 color
plot 'test1obs_jco-5-times.txt' u 1:(1e3*$2) t "L ({/Symbol \264} 10^{-3})" w l lw 4, \
     'test1obs_jco-5-times.txt' u 1:(1e2*$3) t "L_e ({/Symbol \264} 10^{-2})" w l lw 4, \
     'test1obs_jco-5-times.txt' u 1:(1e3*$9) t "L_{obs} ({/Symbol \264} 10^{-3})" w l lw 4, \
     'test1obs_jco-5-times.txt' u 1:(1e4*$8) t "L_{e,obs} ({/Symbol \264} 10^{-4})" w l lw 4

set xrange [0:18]
unset label
set label "(d)" at graph 0.05,0.05
set ylabel "Sensitivity\t\t|J_{{/Symbol q}_m}|" font "Helvetica,28"
set yrange [1.0E-7:0.1]
set logscale y
set ylabel offset -0.5,0

set ytics 1.0E-9,10,0.01
set key horiz top right
set format y "10^{%L}"
set output 'p13mc1-5sen2-log.eps'
plot 'test1obs_jco-5-times.txt' u 1:(abs($2)) t "L" w l lw 4, \
     'test1obs_jco-5-times.txt' u 1:(abs($3)) t "L_e" w l lw 4, \
     'test1obs_jco-5-times.txt' u 1:(abs($9)) t "L_{obs}" w l lw 4, \
     'test1obs_jco-5-times.txt' u 1:(abs($8)) t "L_{e,obs}" w l lw 4
