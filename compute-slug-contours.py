import numpy as np
from matplotlib import pyplot as plt
import matplotlib.colors as mc
from subprocess import call

inputString = """T  F  F                  	     :: suppress output to screen?, output dimensionless t?, log output?
5.81  F  2.10000000000000 2.200000000000000178               :: saturated thickness (B), compute L/Le?, L, Le
0.0065 1.000000000000 2.249597104450486          :: robs, F, Ks
%.5e   %.5e              :: r, z observation locations
2.165  1.815     	     :: l,d; depth to bottom, top of test interval
0.0315  0.0155     	     :: rw,rc; wellbore, tubing radius
2.249597104450486 .003174334374142542      2.249597104450486   0.02 2.832 0.01       .003174334374142542  :: K1,2,3;del1,2,3;Kz
2.303890793654702E-4 .358540375410345169         :: Ss,Sy
9.81D+0  1.20D-6             :: g,nu; grav acc, kinematic viscosity at 15C
40  1.0D-8  1.0D-9           :: deHoog invlap;  M,alpha,tol
7  4                         :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
1  1  12  100                :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
-2  2  %i                   :: logspace times; lo, hi, # times to compute
t  141  times.dat             :: compute times?, # times to read, filename for times (1 time/line)
test.out
"""

recompute = False

nr = 30
rw = 0.0315
rvec = np.linspace(rw,5.0,nr)

nz = 20
zvec = np.linspace(0.0,5.81,nz)

nt = 20
res = np.zeros((nr,nz,nt))
dres = np.zeros((nr,nz,nt))

if recompute:
    for i,r in enumerate(rvec):
        for j,z in enumerate(zvec):
            
            print i,j
            fh = open('input-gen.dat','w')
            fh.write(inputString % (r,z,nt))
            fh.close()
            call('./genslug')
            
            t,p,dp = np.loadtxt('test.out',skiprows=18,unpack=True)
            
            res[i,j,:] = p
            dres[i,j,:] = dp

    np.savez('res.npz',res)
    np.savez('dres.npz',dres)
else:
    print 'reading files of saved data'
    res = np.load('res.npz')['arr_0']
    dres = np.load('dres.npz')['arr_0']
    t = np.logspace(-2,2,nt)

Z,R = np.meshgrid(zvec,rvec)

print 'R,Z',R.shape,Z.shape

# global color scale across all times
pmin = np.min(res)
pmax = np.max(res)
nrm1 = mc.Normalize(vmin=pmin,vmax=pmax)
cm1 = mc.LinearSegmentedColormap.from_list('cm1',[(pmin,'red'),(0.0,'white'),(pmax,'blue')])

dpmin = np.min(dres)
dpmax = np.max(dres)
nrm2 = mc.Normalize(vmin=dpmin,vmax=dpmax)
cm2 = mc.LinearSegmentedColormap.from_list('cm2',[(dpmin,'red'),(0.0,'white'),(dpmax,'blue')])

for i in range(nt):
    print i
    fig = plt.figure(1,figsize=(14,6))
    axp = fig.add_subplot(121)

    #C = axp.contourf(R,Z,np.log(np.abs(res[:,:,i])),20)
    C = axp.contourf(R,Z,res[:,:,i],20,norm=nrm1,cm=cm1)
    axp.set_xlabel('$r$')
    axp.set_ylabel('$z$')
    axp.set_title('$p$ $t=$%.2g' % t[i])
    plt.colorbar(C,ax=axp,shrink=1.0)


    axdp = fig.add_subplot(122)
    #C = axdp.contourf(R,Z,np.log(np.abs(dres[:,:,i])),20) 
    C = axdp.contourf(R,Z,dres[:,:,i],20,norm=nrm2,cm=cm2)
    axdp.set_xlabel('$r$')
    axdp.set_ylabel('$z$')
    axdp.set_title('$dp$ $t=$%.2g' % t[i])
    plt.colorbar(C,ax=axdp,shrink=1.0)
    plt.savefig('contours-t-%2.2i-%.2g.png' % (i,t[i]))
    plt.close(1)
    
    
    
